/**
 * Created by Asus on 07/06/2017.
 */

import LoginState from '../../states/login.state.js';

export default {
    data(){
        return {
            sharedState: LoginState.state
        }
    },
    mounted(){
        //logout();
        sessionStorage.removeItem("salt");
        this.sharedState.login = false;
        this.sharedState.username = '';
        this.$router.push({path: '/'});
    }

}