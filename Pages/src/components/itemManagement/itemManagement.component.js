/**
 * Created by Asus on 21/05/2017.
 */
import Operator from '../../utils/model/operator/index';

import ItemModel from '../../models/item/item.model.js';
import ItemService from '../../services/item/item.service';
import ItemComponent from '../item/item.component.js'

import LoadingPanel from '../commons/loading-panel/loading-panel.common.vue';
import ErrorPanel from '../commons/error-panel/error-panel.common.vue';
import Toast from '../commons/toast/toast.common.vue';

export default {
    components: {
        'loading-panel': LoadingPanel,
        'error-panel': ErrorPanel,
        'toast': Toast
    },
    data() {
        return {
            message: 'Manage Items',
            description: 'Manage your items here',
            itemConfig: {
                loading: true,
                error: false
            },
            item: ItemModel,
            items: []
            //habis itu diaini ada useridnya, terus pas bind() cuma pilih yang ada userIdnya
        }
    },
    //sekali jalan / constructor
    mounted() {
        this.bindItems();

    },
    methods: {
        //get data
        bindItems() {
            this.itemConfig.loading = true;
            this.itemConfig.error = false;

            ItemService.fetch(this)
                .then(
                    res => {
                        this.items = Operator.map(ItemModel, res.data);
                        this.itemConfig.loading = false;
                    },
                    err => {
                        this.itemConfig.loading = false;
                        this.itemConfig.error = true;
                    }
                );
        },
        storeItem() {
            ItemService.store(this, this.item)
                .then(
                    res => {
                        this.items.push(Operator.single(ItemModel, res.data));
                        this.item = Operator.reset(ItemModel);
                        this.bindItems();
                    },
                    err => {
                        this.$refs.toast.setMessage('Error store item, check your item input again.');
                        this.$refs.toast.show();
                    }
                )
        },
        deleteItem(item) {
            ItemService.delete(this, item.id)
                .then(
                    res => {
                        this.items.splice(this.items.indexOf(item), 1);
                    },
                    err => {
                        this.$refs.toast.setMessage('Error delete item');
                        this.$refs.toast.show();
                    }
                )
        },
        editItem(item) {
            this.items[this.items.indexOf(item)].onedit = true;
        },
        updateItem(item) {
            ItemService.update(this, item.id, item)
                .then(
                    res => {
                        this.items[this.items.indexOf(item)].onedit = false;
                    },
                    err => {
                        this.$refs.toast.setMessage('Error update item');
                        this.$refs.toast.show();
                    }
                );
        }
    }
}
