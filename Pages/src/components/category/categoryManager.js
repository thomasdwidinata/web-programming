/**
 * Created by Asus on 21/05/2017.
 */
import Operator from '../../utils/model/operator/index';

import CategoryModel from '../../models/category/category.model.js';
import CategoryService from '../../services/category/category.service';
//import CategoryComponent from '../item/item.component.js'

import LoadingPanel from '../commons/loading-panel/loading-panel.common.vue';
import ErrorPanel from '../commons/error-panel/error-panel.common.vue';
import Toast from '../commons/toast/toast.common.vue';

export default {
    components: {
        'loading-panel': LoadingPanel,
        'error-panel': ErrorPanel,
        'toast': Toast
    },
    data() {
        return {
            message: 'Manage Items',
            description: 'Manage your items here',
            categoryConfig: {
                loading: true,
                error: false
            },
            category: CategoryModel,
            categories: []
            //habis itu disini ada useridnya, terus pas bind() cuma pilih yang ada userIdnya
        }
    },
    //sekali jalan / constructor
    mounted() {
        this.bindCategories();

    },
    methods: {
        //get data
        bindCategories() {
            this.categoryConfig.loading = true;
            this.categoryConfig.error = false;

            CategoryService.fetch(this)
                .then(
                    res => {
                        this.items = Operator.map(ItemModel, res.data);
                        this.categoryConfig.loading = false;
                    },
                    err => {
                        this.categoryConfig.loading = false;
                        this.categoryConfig.error = true;
                    }
                );
        },
        storeCategory() {
            ItemService.store(this, this.category)
                .then(
                    res => {
                        this.categories.push(Operator.single(CategoryModel, res.data));
                        this.category = Operator.reset(CategoryModel);
                        this.bindItems();
                    },
                    err => {
                        this.$refs.toast.setMessage('Error store category, check your item input again.');
                        this.$refs.toast.show();
                    }
                )
        },
        deleteCategory(category) {
            ItemService.delete(this, category.id)
                .then(
                    res => {
                        this.items.splice(this.categories.indexOf(category), 1);
                    },
                    err => {
                        this.$refs.toast.setMessage('Error delete category');
                        this.$refs.toast.show();
                    }
                )
        },
        editCategory(category) {
            this.items[this.categories.indexOf(category)].onedit = true;
        },
        updateCategory(category) {
            CategoryService.update(this, category.id, category)
                .then(
                    res => {
                        this.categories[this.categories.indexOf(category)].onedit = false;
                    },
                    err => {
                        this.$refs.toast.setMessage('Error update category');
                        this.$refs.toast.show();
                    }
                );
        }
    }
}
