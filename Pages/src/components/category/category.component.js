/**
 * Created by Asus on 21/05/2017.
 */
import Operator from '../../utils/model/operator/index';

import CategoryModel from '../../models/category/category.model.js';
import CategoryService from '../../services/category/category.service.js';

import LoadingPanel from '../commons/loading-panel/loading-panel.common.vue';
import ErrorPanel from '../commons/error-panel/error-panel.common.vue';
import Toast from '../commons/toast/toast.common.vue';
import BrowsingState from '../../states/browsing.state.js'

export default {
    components: {
        'loading-panel': LoadingPanel,
        'error-panel': ErrorPanel,
        'toast': Toast
    },
    data() {
        return {
            message: 'All Categories',
            description: 'View all categories at this page.',
            categoryConfig: {
                loading: true,
                error: false
            },
            category: CategoryModel,
            categories: [],
            keyWord:'',
            browsingState: BrowsingState.state
        }
    },
    //sekali jalan / constructor
    mounted() {
        this.bindCategory();
    },
    methods: {
        //get data
        bindCategory() {
            this.categoryConfig.loading = true;
            this.categoryConfig.error = false;

            CategoryService.fetch(this)
                .then(
                    res => {
                        this.categories = Operator.map(CategoryModel, res.data);
                        this.categoryConfig.loading = false;
                    },
                    err => {
                        this.categoryConfig.loading = false;
                        this.categoryConfig.error = true;
                    }
                );
        },
        selectCategory(category){
            this.browsingState.categoryName = category.categoryName;
        }
        // categoryByName(){
        //     this.categoryConfig.loading = true;
        //     this.categoryConfig.error = false;
        //
        //     CategoryService.findByName(this, this.keyWord)
        //         .then(
        //             res => {
        //                 this.categories = Operator.map(CategoryModel, res.data);
        //                 this.categoryConfig.loading = false;
        //             },
        //             err => {
        //                 this.categoryConfig.loading = false;
        //                 this.categoryConfig.error = true;
        //             }
        //         );
        // }

        //,
        // storeUser() {
        //     UserService.store(this, this.user)
        //         .then(
        //             res => {
        //                 this.users.push(Operator.single(UserModel, res.data));
        //                 this.user = Operator.reset(UserModel);
        //                 this.bindUsers();
        //             },
        //             err => {
        //                 this.$refs.toast.setMessage('Error store user, check your user input again.');
        //                 this.$refs.toast.show();
        //             }
        //         )
        // },
        // deleteUser(user) {
        //     UserService.delete(this, user.id)
        //         .then(
        //             res => {
        //                 this.users.splice(this.users.indexOf(user), 1);
        //             },
        //             err => {
        //                 this.$refs.toast.setMessage('Error delete user');
        //                 this.$refs.toast.show();
        //             }
        //         )
        // },
        // editUser(user) {
        //     this.users[this.users.indexOf(user)].onedit = true;
        // },
        // updateUser(user) {
        //     UserService.update(this, user.id, user)
        //         .then(
        //             res => {
        //                 this.users[this.users.indexOf(user)].onedit = false;
        //             },
        //             err => {
        //                 this.$refs.toast.setMessage('Error update user');
        //                 this.$refs.toast.show();
        //             }
        //         );
        // }
    }
}
