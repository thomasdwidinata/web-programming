/**
 * Created by Asus on 07/06/2017.
 */
import Operator from '../../utils/model/operator/index';

import LoginService from '../../services/login/login.service.js';
import LoadingPanel from '../commons/loading-panel/loading-panel.common.vue';
import ErrorPanel from '../commons/error-panel/error-panel.common.vue';
import Toast from '../commons/toast/toast.common.vue';

import  LoginState from '../../states/login.state.js';

export default {
    components: {
        'loading-panel': LoadingPanel,
        'error-panel': ErrorPanel,
        'toast': Toast
    },
    data() {
        return {
           // username: LoginService.getUserInfo(),
            salt: '',
            username: LoginState.state.username,
            sharedState : LoginState.state
        }
    },
    //sekali jalan / constructor
    mounted() {
        console.log("in login snippet: "+this.username);
        this.username  = LoginService.getUserInfo();
    },
    methods: {
    }

}
