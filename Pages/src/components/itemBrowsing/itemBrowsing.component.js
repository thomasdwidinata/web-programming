/**
 * Created by Asus on 23/05/2017.
 */
import Category from '../category/category.component.vue';
import Item from '../item/item.component.vue';
import BrowsingState from "../../states/browsing.state.js"

export default {
    components: {
        'category': Category,
        'item': Item
    },
    data(){
        return {
            message:'search items',
            description:'',
            browsingState: BrowsingState.state
        }
    },
    mounted(){
    },
    methods: {
    }
}