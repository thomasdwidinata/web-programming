/**
 * Created by Asus on 21/05/2017.
 */
import Operator from '../../utils/model/operator/index';

import ItemModel from '../../models/item/item.model.js';
import ItemService from '../../services/item/item.service';

import LoadingPanel from '../commons/loading-panel/loading-panel.common.vue';
import ErrorPanel from '../commons/error-panel/error-panel.common.vue';
import Toast from '../commons/toast/toast.common.vue';

export default {
    components: {
        'loading-panel': LoadingPanel,
        'error-panel': ErrorPanel,
        'toast': Toast
    },
    data() {
        return {
            message: 'All Items',
            description: 'View all items at this page.',
            itemConfig: {
                loading: true,
                error: false
            },
            item: ItemModel,
            items: []
        }
    },
    //sekali jalan / constructor
    mounted() {
        this.bindItems();
    },
    methods: {
        //get data
        bindItems(){
            this.itemConfig.loading = true;
            this.itemConfig.error = false;

            if (this.keyword =='') {
                console.log("empty keyword");
            }
            else{
                console.log("has keyword");
                console.log(this.keyword);
            }
            ItemService.fetch(this)
                .then(
                    res => {
                        this.items = Operator.map(ItemModel, res.data);
                        this.itemConfig.loading = false;
                    },
                    err => {
                        this.itemConfig.loading = false;
                        this.itemConfig.error = true;
                    }
                );

         },
        itemByName(){
            this.itemConfig.loading = true;
            this.itemConfig.error = false;

            ItemService.findByName(this, this.keyWord)
                .then(
                    res => {
                        this.items = Operator.map(ItemModel, res.data);
                        this.itemConfig.loading = false;
                    },
                    err => {
                        this.itemConfig.loading = false;
                        this.itemConfig.error = true;
                    }
                );
        },
        storeItem() {
            ItemService.store(this, this.user)
                .then(
                    res => {
                        this.items.push(Operator.single(UserModel, res.data));
                        this.item = Operator.reset(UserModel);
                        this.bindItems();
                    },
                    err => {
                        this.$refs.toast.setMessage('Error store item, check your item input again.');
                        this.$refs.toast.show();
                    }
                )
        },
        deleteUser(item) {
            ItemService.delete(this, item.id)
                .then(
                    res => {
                        this.items.splice(this.items.indexOf(item), 1);
                    },
                    err => {
                        this.$refs.toast.setMessage('Error delete item');
                        this.$refs.toast.show();
                    }
                )
        },
        editUser(item) {
            this.items[this.items.indexOf(item)].onedit = true;
        },
        updateUser(item) {
            ItemService.update(this, item.id, item)
                .then(
                    res => {
                        this.items[this.items.indexOf(item)].onedit = false;
                    },
                    err => {
                        this.$refs.toast.setMessage('Error update user');
                        this.$refs.toast.show();
                    }
                );
        }
    },
    props:['keyword','categoryName']


}
