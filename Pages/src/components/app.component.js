import LoginSnippet from './loginSnippet/loginSnippet.component.vue';
import BrowsingState from '../states/browsing.state.js'

export default {
    components:{
        'login-snippet': LoginSnippet
    },
    data() {
        return {
            browsingState: BrowsingState.state,
            keyWord: ''
        }
    },
    methods:{
        search(){
            //this.keyWord= document.getElementById("search-keyword").value;
            console.log("retrieved keyword: "+this.keyWord);
            this.browsingState.keyword = this.keyWord;
            // this.$router.push({path: 'browse/'});
            this.$router.replace('/browse');
        }
    },
    props: ['authentication','userId']

}