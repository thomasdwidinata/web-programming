/**
 * Created by Asus on 19/05/2017.
 */
import Operator from '../../utils/model/operator/index';

import LoginService from '../../services/login/login.service.js';

import LoadingPanel from '../commons/loading-panel/loading-panel.common.vue';
import ErrorPanel from '../commons/error-panel/error-panel.common.vue';
import Toast from '../commons/toast/toast.common.vue';

import LoginState from '../../states/login.state.js';

export default {
    components: {
        'loading-panel': LoadingPanel,
        'error-panel': ErrorPanel,
        'toast': Toast
    },
    data() {
        return {
            message: 'Login Page',
            description: "Welcome back new user? register here",
            userConfig: {
                loading: true,
                error: false
            },
            //user: UserModel,
            //users: [],
            user: {
                userName: '',
                password: ''
            },
            salt: '',
            sharedState: LoginState.state
        }
    },
    //sekali jalan / constructor
    mounted() {
        console.log("checking salt: "+sessionStorage.getItem("salt"));
    },
    methods: {
        login(){
            console.log("in the component: "+this.user.toString());
            LoginService.login(this, this.user).then(
              res=>{
                  this.salt = res.bodyText;
                  console.log("trying to log...");
                  console.log("res: "+res.toString());
                  sessionStorage.setItem('salt', res.bodyText);
                  console.log("ini login: "+sessionStorage.getItem('salt'));
                  this.sharedState.login = true;
                  this.sharedState.username = LoginService.getUserInfo();
                  console.log("get username: "+this.sharedState.username);
                  this.$router.push ({path: '/'});
              },
              err=>{
                  console.log("trying to log...");
                  console.log("error");
              }
          );
           //UserService.login();
            alert("logged in!");
        },
        getUserInfo(){
            return this.user.userName;
        }
    }
}
