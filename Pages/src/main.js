import scss from './assets/stylesheets/app.scss'; //css

import Vue from 'vue';
// import Vuex from 'states';
import Router from 'vue-router';
import Resource from 'vue-resource';

Vue.use(Router);
Vue.use(Resource);
// Vue.use(Vuex);

import AppComponent from './components/app.component.vue';

//controller + view
import HomeComponent from './components/home/home.component.vue';
import UserComponent from './components/user/user.component.vue';
import LoginComponent from './components/login/login.component.vue';
import ItemComponent from './components/item/item.component.vue';
import CategoryComponent from './components/category/category.component.vue';
import ItemBrowsingComponent from './components/itemBrowsing/itemBrowsing.component.vue';
import ItemManagement from './components/itemManagement/itemManagement.component.vue';
import ShoppingCart from './components/shoppingCart/shoppingCart.component.vue';
import WishList from './components/wishlist/wishlist.component.vue';
import Thread from './components/thread/thread.component.vue';
import Logout from './components/logout/logout.component.vue';

const routes = [
    {
        path: '/',
        component: HomeComponent
    },
    {
        path: '/user',
        component: UserComponent
    },
    {
        path: '/login',
        component: LoginComponent
    },
    {
        path: '/item',
        component: ItemComponent
    },
    {
        path:'/category',
        component: CategoryComponent
    },
    {
        path: '/browse',
        component: ItemBrowsingComponent,
        //props: {result: this.}
    },
    {
        path: '/browse/:keyWord',
        component: ItemBrowsingComponent
    },
    {
        path: '/item/add',
        component:ItemManagement
    },
    {
        path: '/cart',
        component:ShoppingCart
    },
    {
        path: '/wishlist',
        component:WishList
    },
    {
        path: '/thread',
        component:Thread
    },
    {
        path: '/logout',
        component:Logout
    }
];

const router = new Router({
    routes: routes
});

new Vue({
    router: router,
    render: h => h(AppComponent)
}).$mount('#app');
