import Config from '../config';

export default {
    url: Config.api + '/user',
    fetch(v) {
        return v.$http.get(this.url);//ambil data dari localhostxxx
    },
    find(v, id) {
        return v.$http.get(this.url + '/' + id);
    },
    //login pake store (POST)
    store(v, params) {
       return v.$http.post(this.url, params);
    },
    delete(v, id) {
        return v.$http.delete(this.url + '/' + id);
    },
    update(v, id, params) {
        return v.$http.patch(this.url + '/' + id, params);
    }
    // login(){
    //     console.log("login...");
    //     return "lul";
    // }
}
