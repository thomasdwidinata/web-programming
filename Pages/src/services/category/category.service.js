/**
 * Created by Asus on 22/05/2017.
 */

import Config from '../config';

export default {
    url: Config.api + '/category',
    fetch(v) {
        return v.$http.get(this.url);//ambil data dari localhostxxx
    },
    find(v, id) {
        return v.$http.get(this.url + '/' + id);
    },
    // findByName (y,name){
    //     return y.$http.get(this.url + '/byName/' + name);
    // },
    //login pake store (POST)
    store(v, params) {
        return v.$http.post(this.url, params);
    },
    delete(v, id) {
        return v.$http.delete(this.url + '/' + id);
    },
    update(v, id, params) {
        return v.$http.patch(this.url + '/' + id, params);
    }
}
