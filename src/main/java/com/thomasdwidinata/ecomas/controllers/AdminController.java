package com.thomasdwidinata.ecomas.controllers;

import com.thomasdwidinata.ecomas.models.Admin;
import com.thomasdwidinata.ecomas.services.AdminRoleServices;
import com.thomasdwidinata.ecomas.services.AdminServices;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author thomasdwidinata
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@ComponentScan("com.thomasdwidinata.ecomas.services")
public class AdminController {
    
    @Autowired
    private AdminServices adminServices;
    
    @RequestMapping(value="/admin", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createItemWithNewCategoryPOST(@RequestBody Map<String, Object> data)
    {
        adminServices.add(data.get("adminName").toString(), data.get("password").toString(), data.get("adminRole").toString());
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
    
    @RequestMapping(value="/admin/{adminId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> delete(@PathVariable("adminId") String id)
    {
        adminServices.delete(Integer.valueOf(id));
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<Void>(headers, HttpStatus.ACCEPTED);
    }
    
    @RequestMapping(value="/admin", method=RequestMethod.GET)
    public ResponseEntity<List<Admin>> get(ModelMap models){
        List<Admin> users = adminServices.get();
        if(users.isEmpty()){
            return new ResponseEntity<List<Admin>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Admin>>(users, HttpStatus.OK);
    }
    
    @RequestMapping(value="/admin/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Object> update(@PathVariable("id") String id,@RequestBody Map<String, Object> data){
        Admin currentAdmin = adminServices.get(Integer.valueOf(id));
        if(currentAdmin == null){
            System.out.println("Admin with ID : " + id + " was not found!");
            return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
        }
        adminServices.update(Integer.valueOf(id), data);
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<Object>(headers, HttpStatus.OK);
    }
    
}
