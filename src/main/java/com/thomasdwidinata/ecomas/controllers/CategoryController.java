package com.thomasdwidinata.ecomas.controllers;

import com.thomasdwidinata.ecomas.models.Category;
import com.thomasdwidinata.ecomas.services.CategoryServices;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author thomasdwidinata
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@ComponentScan("com.thomasdwidinata.ecomas.services")
public class CategoryController {
    @Autowired
    private CategoryServices catServices;
    
    // Get all Categories
    @RequestMapping(value="/category", method=RequestMethod.GET)
    public ResponseEntity<List<Category>> listCategory(ModelMap models){
        List<Category> users = catServices.findAllCategory();
        if(users.isEmpty()){
            return new ResponseEntity<List<Category>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Category>>(users, HttpStatus.OK);
    }
    
    // Search Category by ID
    @RequestMapping(value="/category/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Category> getCategoryByID(@PathVariable("id") String id){
        Category cat =  catServices.findById(Integer.valueOf(id));
        if(cat == null){
            System.out.println("Item with ID : " + id + " not found!");
            return new ResponseEntity<Category>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Category>(cat, HttpStatus.OK);
    }
    
    // Search Category by Name
    @RequestMapping(value="/category/byName/{name}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Category> getCategoryByName(@PathVariable("name") String name){
        Category cat =  catServices.findByName(name);
        if(cat == null){
            System.out.println("Category with Name : " + name + " was not found!");
            return new ResponseEntity<Category>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Category>(cat, HttpStatus.OK);
    }
    
    // Insert new Category
    @RequestMapping(value="/category", method=RequestMethod.POST)
    public ResponseEntity<Void> createCategory(@RequestBody Map<String, Object> data){
        catServices.addCategory(data.get("categoryName").toString());
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
    
    // Update new Category using ID and POST new Data
    @RequestMapping(value="/category/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Category> updateCategory(@PathVariable("id") String id, @RequestBody Map<String, Object> data){
        System.out.println("Updating Category : " + id);
        Category currentCategory = catServices.findById(Integer.valueOf(id));
        if(currentCategory == null){
            System.out.println("Category with ID : " + id + " was not found!");
            return new ResponseEntity<Category>(HttpStatus.NOT_FOUND);
        }
        catServices.updateCategory(Integer.valueOf(id), data);
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<Category>(headers, HttpStatus.OK);
    }
    
    // Delete a Category using ID
    @RequestMapping(value="/category/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Category> removeCategory(@PathVariable("id") String id){
        Category cat = catServices.findById(Integer.valueOf(id));
        if(cat == null){
            System.out.println("Unable to delete. Category with ID : " + id + " was not found!");
            return new ResponseEntity<Category>(HttpStatus.NOT_FOUND);
        }
        catServices.deleteCategoryById(Integer.valueOf(id));
        return new ResponseEntity<Category>(HttpStatus.NO_CONTENT);
    }
}
