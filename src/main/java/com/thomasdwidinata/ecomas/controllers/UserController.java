package com.thomasdwidinata.ecomas.controllers;

import com.thomasdwidinata.ecomas.services.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.thomasdwidinata.ecomas.models.User;
import java.util.List;
import java.util.Map;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author thomasdwidinata
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@ComponentScan("com.thomasdwidinata.ecomas.services")
public class UserController {
    
    @Autowired
    private UserServices userServices;
    
    @RequestMapping(value="/", method = RequestMethod.GET)
    public String getIndexPage(ModelMap models){
        List<User> users = userServices.get();
        models.put("listUser", users);
        return "index";
    }
    
    @RequestMapping(value="/user/{id}", method = RequestMethod.GET)
    public String updateUser(@PathVariable("id") String id, ModelMap modelMap){
        modelMap.put("id", id);
        return "user/update";
    }
    
    @RequestMapping(value="/user", method=RequestMethod.GET)
    public ResponseEntity<List<User>> listUsers(ModelMap models){
        List<User> users = userServices.get();
        if(users.isEmpty()){
            return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<User>>(users, HttpStatus.OK);
    }
    
    @RequestMapping(value="/user/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> getUser(@PathVariable("id") String id){
        User user = userServices.get(Integer.valueOf(id));
        if(user == null){
            System.out.println("USer with ID : " + id + " not found!");
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }
    
    @RequestMapping(value="/register", method = RequestMethod.POST)
    public ResponseEntity<Void> createuser(@RequestBody Map<String, Object> data, UriComponentsBuilder ucBuilder){
        String username = data.get("username").toString();
        String email = data.get("email").toString();
        String password = data.get("password").toString();
//        String role = data.get("role").toString();
        String address = data.get("address").toString();
        System.out.println("Creating user " + username);
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setEmail(email);
        user.setAddress(address);
//        user.setRole(role);
        userServices.add(user);
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
    
    @RequestMapping(value="/login", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> login(@RequestBody Map<String, Object> data, UriComponentsBuilder ucBuilder){
        String username = data.get("username").toString();
        String password = data.get("password").toString();
        System.out.println("Logging in... " + username);
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        String salt = userServices.auth(user);
        System.out.println("salt for " + username + " : " + salt);
        return new ResponseEntity<String>(salt, HttpStatus.OK);
    }
    
    @RequestMapping(value="/getUserInfo", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<User> getInfo(@RequestBody Map<String, Object> data, UriComponentsBuilder ucBuilder){
        User u = userServices.matchSalt(data.get("salt").toString());
        return new ResponseEntity<User>(u, HttpStatus.CREATED);
    }
    
    @RequestMapping(value="/user/{id}", method = RequestMethod.PATCH)
    public ResponseEntity<User> updateUser(@PathVariable("id") String id, @RequestBody Map<String, Object> data){
        System.out.println("Updating User : " + id);
        User currentUser = userServices.get(Integer.valueOf(id));
        if(currentUser == null){
            System.out.println("User with ID : " + id + " was not found!");
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        userServices.update(Integer.valueOf(id), data);
        return new ResponseEntity<User>(currentUser, HttpStatus.OK);
    }
    @RequestMapping(value="/user/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<User> removeUser(@PathVariable("id") String id){
        User user = userServices.get(Integer.valueOf(id));
        if(user == null){
            System.out.println("Unable to delete. User with ID : " + id + " was not found!");
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        userServices.delete(Integer.valueOf(id));
        return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
    }
    
    
}