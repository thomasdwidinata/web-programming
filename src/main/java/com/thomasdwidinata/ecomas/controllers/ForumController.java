package com.thomasdwidinata.ecomas.controllers;

import java.util.List;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author thomasdwidinata
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@ComponentScan("com.thomasdwidinata.ecomas.services")
public class ForumController {
//    @RequestMapping(value = "", method = RequestMethod.GET)
//    public ResponseEntity<List<ForumMessage> getMessage()
}
