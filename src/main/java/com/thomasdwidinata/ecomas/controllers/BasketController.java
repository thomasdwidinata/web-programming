package com.thomasdwidinata.ecomas.controllers;

import com.thomasdwidinata.ecomas.models.Basket;
import com.thomasdwidinata.ecomas.models.BasketItems;
import com.thomasdwidinata.ecomas.models.BasketView;
import com.thomasdwidinata.ecomas.services.BasketViewServices;
import com.thomasdwidinata.ecomas.services.BasketItemsServicesImpl;
import com.thomasdwidinata.ecomas.services.BasketServices;
import com.thomasdwidinata.ecomas.services.MagicServices;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author thomasdwidinata
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@ComponentScan("com.thomasdwidinata.ecomas.services")
public class BasketController {
    @Autowired
    private BasketServices basketService;
    
    @Autowired private BasketViewServices bvServices;
    
    @Autowired
    private MagicServices magic;
    
    @Autowired
    private BasketItemsServicesImpl biService;
    
   
    @RequestMapping(value="/basket", method=RequestMethod.POST)
    public ResponseEntity<List<BasketView>> getBySalt(@RequestBody Map<String, Object> data){
        List<BasketView> b = bvServices.get(data);
        if(data.isEmpty()){
            return new ResponseEntity<List<BasketView>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<BasketView>>(b, HttpStatus.OK);
    }
    
    @RequestMapping(value="/basket/add", method=RequestMethod.POST)
    public ResponseEntity<List<Basket>> addBasket(@RequestBody Map<String, Object> data, UriComponentsBuilder ucBuilder){
        basketService.add(data.get("salt").toString(), Integer.valueOf(data.get("itemId").toString()), Integer.valueOf(data.get("stock").toString()));
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<List<Basket>>(headers, HttpStatus.CREATED);
    }
}
