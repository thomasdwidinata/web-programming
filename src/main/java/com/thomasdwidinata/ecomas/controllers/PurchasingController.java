package com.thomasdwidinata.ecomas.controllers;

import com.thomasdwidinata.ecomas.models.TransactionTable;
import com.thomasdwidinata.ecomas.models.Payment;
import com.thomasdwidinata.ecomas.models.Shipping;
import com.thomasdwidinata.ecomas.services.TransactionServices;
import com.thomasdwidinata.ecomas.services.PaymentServices;
import com.thomasdwidinata.ecomas.services.ShippingServices;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;


/**
 *
 * @author thomasdwidinata
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@ComponentScan("com.thomasdwidinata.ecomas.services")
public class PurchasingController {
    @Autowired
    private TransactionServices services;
    
    @Autowired private PaymentServices ps;
    
    @Autowired private ShippingServices ss;
    
    // Insert new Category
    @RequestMapping(value="/purchase", method=RequestMethod.POST)
    public ResponseEntity<Void> add(@RequestBody Map<String, Object> data){
        services.add(data);
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
    
    @RequestMapping(value="/purchase/get", method=RequestMethod.POST)
    public ResponseEntity<Void> get(@RequestBody Map<String, Object> data){
        services.getBasedOnUser(data);
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
    
    @RequestMapping(value="/purchase/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Void> updateCategory(@PathVariable("id") String id, @RequestBody Map<String, Object> data){
        TransactionTable t = services.get(Integer.valueOf(id));
        if(t == null)
        {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        services.update(Integer.valueOf(id), data);
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<Void>(headers, HttpStatus.OK);
    }
    
    @RequestMapping(value="/purchase/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<TransactionTable> removeCategory(@PathVariable("id") String id){
        TransactionTable cat = services.get(Integer.valueOf(id));
        if(cat == null){
            return new ResponseEntity<TransactionTable>(HttpStatus.NOT_FOUND);
        }
        services.delete(Integer.valueOf(id));
        return new ResponseEntity<TransactionTable>(HttpStatus.NO_CONTENT);
    }
    
    @RequestMapping(value = "/payment/getPayment", method = RequestMethod.GET)
    public ResponseEntity<List<Payment>> getPayment(ModelMap models)
    {
        List<Payment> data = ps.get();
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<List<Payment>>(data, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/payment/getShipping", method = RequestMethod.GET)
    public ResponseEntity<List<Shipping>> getShipping(ModelMap models)
    {
        List<Shipping> data = ss.get();
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<List<Shipping>>(data, HttpStatus.OK);
    }
}
