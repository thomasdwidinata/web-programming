package com.thomasdwidinata.ecomas.controllers;

import com.thomasdwidinata.ecomas.models.Item;
import com.thomasdwidinata.ecomas.models.ItemList;
import com.thomasdwidinata.ecomas.services.CategoryServices;
import com.thomasdwidinata.ecomas.services.ItemListServices;
import com.thomasdwidinata.ecomas.services.ItemServices;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author thomasdwidinata
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@ComponentScan("com.thomasdwidinata.ecomas.services")
public class ItemController {
    
    @Autowired
    private ItemListServices itemListServices;
    
    @Autowired
    private ItemServices itemServices;
    
    @Autowired
    private CategoryServices catServices;
    
    // Get All Items
    @RequestMapping(value="/item", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ItemList>> get(ModelMap models){
        List<ItemList> item = itemListServices.get();
        if(item.isEmpty()){
            return new ResponseEntity<List<ItemList>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<ItemList>>(item, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/item/bySeller/{seller}", method=RequestMethod.GET)
    public ResponseEntity<List<ItemList>> getBySeller(@RequestBody Map<String, Object> data)
    {
        return new ResponseEntity<List<ItemList>>(itemListServices.itemSoldByMe(data), HttpStatus.OK);
    }
    
    // Search Item by Name
    @RequestMapping(value="/item/byName/{name}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ItemList>> getCategoryByName(@PathVariable("name") String name){
        List<ItemList> item = itemListServices.get(name);
        if(item == null){
            System.out.println("Item with Name : " + name + " was not found!");
            return new ResponseEntity<List<ItemList>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<ItemList>>(item, HttpStatus.OK);
    }
    
    // Search Item by ID
    @RequestMapping(value="/item/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ItemList> getItem(@PathVariable("id") String id){
        ItemList item = itemListServices.get(Integer.valueOf(id));
        if(item == null){
            System.out.println("Item with ID : " + id + " not found!");
            return new ResponseEntity<ItemList>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<ItemList>(item, HttpStatus.OK);
    }
    
    @RequestMapping(value="/item/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Item> updateItem(@PathVariable("id") String id, @RequestBody Map<String, Object> data){
        System.out.println("Updating Item : " + id);
        Item currentItem = itemServices.findById(Integer.valueOf(id));
        if(currentItem == null){
            System.out.println("Item with ID : " + id + " was not found!");
            return new ResponseEntity<Item>(HttpStatus.NOT_FOUND);
        }
        itemServices.updateItem(Integer.valueOf(id), data);
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<Item>(headers, HttpStatus.OK);
    }
    
    // Delete Item by ID
    @RequestMapping(value="/item/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Item> removeItem(@PathVariable("id") String id){
        Item cat = itemServices.findById(Integer.valueOf(id));
        if(cat == null){
            System.out.println("Unable to delete. Item with ID : " + id + " was not found!");
            return new ResponseEntity<Item>(HttpStatus.NOT_FOUND);
        }
        itemServices.deleteItemById(Integer.valueOf(id));
        return new ResponseEntity<Item>(HttpStatus.NO_CONTENT);
    }
    
    @RequestMapping(value="/item/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> create(@RequestBody Map<String, Object> data)
    {
        itemServices.add(data);
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
}
