package com.thomasdwidinata.ecomas.controllers;

import com.thomasdwidinata.ecomas.services.TransactionServices;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author thomasdwidinata
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@ComponentScan("com.thomasdwidinata.ecomas.services")
public class TransactionValidatorController {
    
    @Autowired
    private TransactionServices services;
    
    @RequestMapping(value="/purchase/validate", method=RequestMethod.POST)
    public ResponseEntity<Void> add(@RequestBody Map<String, Object> data){
        services.add(data);
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
    
//    @RequestMapping(value="/purchase/get", method=RequestMethod.POST)
//    public ResponseEntity<Void> get(@RequestBody Map<String, Object> data){
//        services.getBasedOnUser(data);
//        HttpHeaders headers = new HttpHeaders();
//        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
//    }
}
