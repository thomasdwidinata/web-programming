/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thomasdwidinata.ecomas.services;

import java.util.List;

/**
 *
 * @author thomasdwidinata
 */
public interface ServicesCRUD {
    void add(Object obj);
    List<Object> get();
    void delete(int id);
    void update(Object obj);
}
