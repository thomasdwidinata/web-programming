package com.thomasdwidinata.ecomas.services;

import com.thomasdwidinata.ecomas.dao.AdminDao;
import com.thomasdwidinata.ecomas.dao.AdminRoleDao;
import com.thomasdwidinata.ecomas.models.Admin;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.thomasdwidinata.ecomas.models.AdminRole;
import java.util.Map;

/**
 *
 * @author thomasdwidinata
 */
@Service("AdminServices")
@Transactional
public class AdminServices{
    
    @Autowired
    private AdminDao dao;
    
    @Autowired
    private AdminRoleDao arDao;
    
    public void add(Admin obj){
        dao.persist(obj);
    }
    
    public List<Admin> get()
    {
        return dao.get();
    }
    
    public Admin get(int id)
    {
        return dao.get(id);
    }
    
    public void delete(int id)
    {
        dao.delete(id);
    }
    
    public void update(int id, Map<String, Object> data)
    {
        Admin a = new Admin();
        a.setAdminId(id);
        a.setAdminName(data.get("adminName").toString());
        a.setPassword(data.get("password").toString());
        AdminRole role = arDao.get(data.get("roleName").toString());
        a.setRoleId(role);
        dao.update(a);
    }
    
    public void add(String adminName, String password, String adminRole)
    {
        AdminRole ar = arDao.get(adminRole);
        if(ar == null)
        {
            ar = new AdminRole();
            ar.setRoleName(adminRole);
            arDao.add(ar);
        }

        Admin a = new Admin();
        a.setAdminName(adminName);
        a.setPassword(password);
        a.setRoleId(ar);
        dao.add(a);
        
    }
}
