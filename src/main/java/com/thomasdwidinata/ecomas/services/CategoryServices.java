package com.thomasdwidinata.ecomas.services;

import com.thomasdwidinata.ecomas.models.Category;
import java.util.List;
import java.util.Map;

/**
 *
 * @author thomasdwidinata
 */
public interface CategoryServices {
    void addCategory(String name);
    List<Category> findAllCategory();
    void deleteCategoryById(int id);
    Category findById(int id);
    void updateCategory(int id, Map<String, Object> a);
    Category findByName(String name);
    //Jango -> Python
}
