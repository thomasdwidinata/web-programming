package com.thomasdwidinata.ecomas.services;

import com.thomasdwidinata.ecomas.dao.*;
import com.thomasdwidinata.ecomas.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.thomasdwidinata.ecomas.models.TransactionTable;
import java.util.List;
import java.util.Map;

/**
 *
 * @author thomasdwidinata
 */
@Service("TransactionServices")
@Transactional
public class TransactionServicesImpl implements TransactionServices {
    
    @Autowired
    private TransactionDao dao;
    
    @Autowired
    private BasketDao basketDao;
    
    @Autowired
    private BasketServices basketServices;
    
    @Autowired
    private PaymentDao paymentDao;
    
    @Autowired
    private ShippingDao shippingDao;
    
    @Autowired
    private UserDao userDao;
    
    @Autowired private TransactionViewDao tvDao;
    
    @Override
    public void add(Map<String, Object> data)
    {
        User u = userDao.salt(data.get("salt").toString());
        Basket b = basketServices.getLatest(u.getUsername());
        TransactionTable t = new TransactionTable();
        b.setCheckOut(1);
        t.setBasket(b);
        t.setShipping(shippingDao.get(data.get("shippingName").toString()));
        t.setPaymentId(paymentDao.get(data.get("paymentName").toString()));
        t.setPaymentInfo(data.get("paymentInfo").toString());
        t.setAddress(data.get("address").toString());
        dao.add(t);
        basketDao.add(b);
    }
    
    public List<TransactionTable> get(){
        return dao.get();
    }
    public void delete(int id){
//        User u = userDao.salt(data.get("salt").toString());
//        if(u == null)
//            return;
//        TransactionTable tt = dao.get(id);
//        Basket b = tt.getBasket();
//        User bsu = b.getUser();
//        if(bsu.getUsername().equals(u.getUsername()))
            dao.delete(id);
//        else
//            return;
    }
    public TransactionTable get(int id){
        return dao.get(id);
    }
    public void update(int id, Map<String, Object> data){
        
        TransactionTable t = dao.get(id);
        Basket bt = t.getBasket();
        User u = userDao.salt(data.get("salt").toString());
        if(bt.getUser().getUsername().equals(u.getUsername()))
        {
            t.setShipping(shippingDao.get(data.get("shippingName").toString()));
            t.setPaymentId(paymentDao.get(data.get("paymentName").toString()));
            t.setPaymentInfo(data.get("paymentInfo").toString());
            t.setAddress(data.get("address").toString());
            dao.update(t);
        }
        else
            return;
    }

    @Override
    public void add(TransactionTable data) {
        dao.add(data);
    }
    
    @Override
    public List<TransactionsView> getBasedOnUser(Map<String, Object> data)
    {
        User u = userDao.salt(data.get("salt").toString());
        return tvDao.get(u.getUsername());
    }
}
