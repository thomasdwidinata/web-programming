package com.thomasdwidinata.ecomas.services;

import com.thomasdwidinata.ecomas.dao.BasketDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.thomasdwidinata.ecomas.dao.BasketItemDao;
import com.thomasdwidinata.ecomas.dao.ItemDao;
import com.thomasdwidinata.ecomas.dao.UserDao;
import com.thomasdwidinata.ecomas.models.Basket;
import com.thomasdwidinata.ecomas.models.User;
import com.thomasdwidinata.ecomas.models.BasketItems;
import java.util.List;

/**
 *
 * @author thomasdwidinata
 */
@Service("BasketItemsServices")
@Transactional
public class BasketItemsServicesImpl {
    
    @Autowired
    private BasketItemDao dao;
    
    @Autowired
    private BasketDao bDao;
    
    @Autowired
    private UserDao uDao;
    
    @Autowired
    private ItemDao iDao;
    
    public void add(int basketId, int itemId, int stock){
        BasketItems b = new BasketItems();
        b.setBasket(bDao.get(basketId));
        b.setItem(iDao.findById(itemId));
        dao.add(b);
    }
    
    public void update(BasketItems b)
    {
        dao.update(b);
    }
    
    public void delete(int id)
    {
        dao.delete(id);
    }
    
    public List<BasketItems> get()
    {
        return dao.get();
    }
    
    public List<BasketItems> get(int id)
    {
        return dao.get(id);
    }
}
