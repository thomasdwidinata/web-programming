package com.thomasdwidinata.ecomas.services;

import com.thomasdwidinata.ecomas.models.Category;
import com.thomasdwidinata.ecomas.dao.CategoryDao;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
/**
 *
 * @author thomasdwidinata
 */
@Service("CategoryServices")
@Transactional
public class CategoryServicesImpl implements CategoryServices{
    @Autowired
    private CategoryDao dao;
    
    @Override
    public void addCategory(String name)
    {
        Category cat = new Category();
        cat.setCategoryName(name);
        dao.addCategory(cat);
    }
    
    @Override
    public List<Category> findAllCategory()
    {
        return dao.findAllCategory();
    }
    
    @Override
    public void deleteCategoryById(int id)
    {
        dao.deleteCategoryById(id);
    }
    
    @Override
    public Category findById(int id)
    {
        return dao.findById(id);
    }
    
    @Override
    public void updateCategory(int id, Map<String, Object> data)
    {
        Category c = dao.findById(id);
        c.setCategoryName(data.get("categoryName").toString());
        dao.updateCategory(c);
    }
    
    @Override
    public Category findByName(String name){
        return dao.findCategoryByName(name); 
    }
}
