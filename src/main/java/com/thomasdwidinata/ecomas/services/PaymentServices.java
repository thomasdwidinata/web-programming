package com.thomasdwidinata.ecomas.services;

import java.util.List;
import com.thomasdwidinata.ecomas.models.Payment;

/**
 *
 * @author thomasdwidinata
 */
public interface PaymentServices {
    void add(Payment data);
    List<Payment> get();
    void delete(int id);
    Payment get(int id);
    void update(Payment data);
    Payment get(String name);
}
