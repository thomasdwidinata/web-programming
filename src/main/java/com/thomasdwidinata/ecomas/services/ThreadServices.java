package com.thomasdwidinata.ecomas.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.thomasdwidinata.ecomas.models.*;
import com.thomasdwidinata.ecomas.dao.*;

/**
 *
 * @author thomasdwidinata
 */
@Service("ThreadServices")
@Transactional
public class ThreadServices {
    @Autowired private ThreadMessageDao tmDao;
    @Autowired private ThreadPermissionDao tpDao;
    @Autowired private ThreadTitleDao ttDao;
    
    public void createTitle(String haha)
    {
        ThreadTitle tt = new ThreadTitle();
        tt.setThreadName(haha);
        ThreadType ty = new ThreadType();
        tt.setThreadTypeId(ty);
    }
}
