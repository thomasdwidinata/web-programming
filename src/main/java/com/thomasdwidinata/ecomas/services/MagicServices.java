package com.thomasdwidinata.ecomas.services;

import com.thomasdwidinata.ecomas.dao.AbstractDao;
import com.thomasdwidinata.ecomas.dao.UserDao;
import com.thomasdwidinata.ecomas.models.BasketView;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author thomasdwidinata
 */
@Service("AutoServices")
@Transactional
public class MagicServices extends AbstractDao {
    @Autowired
    private UserDao userDao;
    
    @Transactional
    public void addPurchase(ArrayList<String> alist)
    {
        int a = 0;
        Query q =  getSession().createSQLQuery("INSERT INTO ecomas.transaction (basketId, shippingId, paymentId, paymentInfo, address) VALUES (:basket, :ship, :payment, :paymentInfo, :address);");
        q.setString("basket", alist.get(a++));
        q.setString("ship", alist.get(a++));
        q.setString("payment", alist.get(a++));
        q.setString("paymentInfo", alist.get(a++));
        q.setString("address", alist.get(a++));
//        Query query = session.createQuery("from Resource where emplteam = :team");
//        query.setParameter("team", "Team1");
//        List list = query.list();
    }
    
    public List getCartByUsername(String salt)
    {
        String username = userDao.salt(salt).getUsername();
        Query q = getSession().createSQLQuery("SELECT * FROM `basketview` WHERE username = :usrname");
        q.setString("usrname", username);
        return q.list();
    }
}
