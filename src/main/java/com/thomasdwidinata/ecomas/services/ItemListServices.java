package com.thomasdwidinata.ecomas.services;

import com.thomasdwidinata.ecomas.models.Item;
import com.thomasdwidinata.ecomas.models.ItemList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author thomasdwidinata
 */
public interface ItemListServices {
    List<ItemList> get();
    ItemList get(int id);
    List<ItemList> get(String name);
    List<ItemList> itemSoldByMe(Map<String, Object> data);
}
