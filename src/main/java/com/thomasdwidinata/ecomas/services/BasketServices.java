package com.thomasdwidinata.ecomas.services;

import com.thomasdwidinata.ecomas.models.Basket;
import java.util.List;

/**
 *
 * @author thomasdwidinata
 */
public interface BasketServices {
    void add(String salt, int itemId, int stock);
    List<Basket> get();
    void delete(int id);
    Basket get(int id);
    List<Basket> get(String name);
    void update(Basket b);
    int validity(Basket b);
    Basket getLatest(String name);
}
