package com.thomasdwidinata.ecomas.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.thomasdwidinata.ecomas.dao.*;
import com.thomasdwidinata.ecomas.models.*;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author thomasdwidinata
 */
@Service("BasketViewServices")
@Transactional
public class BasketViewServices {
    @Autowired private BasketViewDao dao;
    @Autowired private UserDao uDao;
    
    public List<BasketView> get()
    {
        return dao.get();
    }
    
    public List<BasketView> get(Map<String, Object> data)
    {
        User u = uDao.salt(data.get("salt").toString());
        return dao.get(u.getUsername());
    }
}
