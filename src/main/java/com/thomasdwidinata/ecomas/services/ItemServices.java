package com.thomasdwidinata.ecomas.services;

import java.util.List;
import com.thomasdwidinata.ecomas.models.Item;
import com.thomasdwidinata.ecomas.models.User;
import java.util.Map;

/**
 *
 * @author thomasdwidinata
 */
public interface ItemServices {
    List<Item> findAllItem();
    void deleteItemById(int id);
    Item findById(int id);
    void updateItem(int id, Map<String, Object> data);
    List<Item> findByName(String item);
    void add(Map<String, Object> data);
}
