package com.thomasdwidinata.ecomas.services;

import org.springframework.beans.factory.annotation.Autowired;
import com.thomasdwidinata.ecomas.models.*;
import com.thomasdwidinata.ecomas.dao.*;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 *
 * @author thomasdwidinata
 */
@Service("ShippingServices")
@Transactional
public class ShippingServices {
    @Autowired private ShippingDao dao;
    
    public List<Shipping> get()
    {
        return dao.get();
    }
    
    
}
