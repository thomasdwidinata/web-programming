package com.thomasdwidinata.ecomas.services;

import com.thomasdwidinata.ecomas.dao.CategoryDao;
import com.thomasdwidinata.ecomas.dao.ItemDao;
import com.thomasdwidinata.ecomas.dao.UnitDao;
import com.thomasdwidinata.ecomas.dao.UserDao;
import com.thomasdwidinata.ecomas.dao.ItemListDao;
import com.thomasdwidinata.ecomas.dao.ItemUomDao;
import com.thomasdwidinata.ecomas.models.User;
import com.thomasdwidinata.ecomas.models.Item;
import com.thomasdwidinata.ecomas.models.ItemUom;
import com.thomasdwidinata.ecomas.models.Category;
import com.thomasdwidinata.ecomas.models.Unit;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author thomasdwidinata
 */

@Service("ItemServices")
@Transactional
public class ItemServicesImpl implements ItemServices{
    @Autowired
    private ItemDao dao;
    
    @Autowired ItemListDao ild;
    
    @Autowired
    private CategoryDao catDao;
    
    @Autowired private UnitDao uDao;
    
    @Autowired UserDao userDao;
    
    @Autowired ItemUomDao iud;
    
    @Override
    public List<Item> findAllItem(){
        return dao.findAllItem();
    }
    
    @Override
    public void deleteItemById(int id){
//        User u = userDao.salt(data.get("salt").toString());
//        Item i = dao.findById(id);
//        if(i.getSeller().getUsername().equals(u.getUsername()))
            dao.deleteItemById(id);
//        else
//            return;
    }
    
    @Override
    public Item findById(int id){
        return dao.findById(id);
    }
    
    @Override
    public void updateItem(int id, Map<String, Object> data){
        Category c = catDao.findCategoryByName(data.get("categoryName").toString());
        
        if(c == null) catDao.addCategory(c);
        
        User seller = userDao.salt(data.get("salt").toString());
        
        Item i = dao.findById(id);
        i.setCategory(c);
        i.setItemName(data.get("itemName").toString());
        i.setSeller(seller);
        ItemUom iu = new ItemUom();
        dao.updateItem(i);
        i = dao.findByNameSpecific(data.get("itemName").toString());
        i.setId(i.getId());
        iu.setItem(i);
        Unit u = uDao.get(data.get("unitName").toString());
        iu.setUnit(u);
        iu.setQty(Integer.valueOf(data.get("qty").toString()));
        iu.setPrice(Double.valueOf(data.get("price").toString()));
        iud.update(iu);
    }
    
    @Override
    public List<Item> findByName(String item){
        return dao.findByName(item);
    }
    
    @Override
    public void add(Map<String, Object> data){
        Category c = catDao.findCategoryByName(data.get("categoryName").toString());
        
        if(c == null) catDao.addCategory(c);
        
        User seller = userDao.salt(data.get("salt").toString());
        
        Item i = new Item();
        i.setCategory(c);
        i.setItemName(data.get("itemName").toString());
        i.setSeller(seller);
        ItemUom iu = new ItemUom();
        dao.addItem(i);
        i = dao.findByNameSpecific(data.get("itemName").toString());
        i.setId(i.getId());
        iu.setItem(i);
        Unit u = uDao.get(data.get("unitName").toString());
        iu.setUnit(u);
        iu.setQty(Integer.valueOf(data.get("qty").toString()));
        iu.setPrice(Double.valueOf(data.get("price").toString()));
        iud.add(iu);
        
    }
}
