package com.thomasdwidinata.ecomas.services;

import com.thomasdwidinata.ecomas.dao.ItemListDaoImpl;
import com.thomasdwidinata.ecomas.models.ItemList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author thomasdwidinata
 */
@Service("ItemListServices")
@Transactional
public class ItemListServicesImpl implements ItemListServices {
    
    @Autowired
    private ItemListDaoImpl dao;
    
    @Override
    public List<ItemList> get()
    {
        return dao.get();
    }

    @Override
    public ItemList get(int id)
    {
        return dao.get(id);
    }

    @Override
    public List<ItemList> get(String name)
    {
        return dao.get(name);
    }
    @Override
    public List<ItemList> itemSoldByMe(Map<String, Object> data)
    {
        return dao.getMySoldItems(data.get("soldBy").toString());
    }
}
