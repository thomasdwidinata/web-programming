package com.thomasdwidinata.ecomas.services;

import com.thomasdwidinata.ecomas.dao.AdminRoleDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.thomasdwidinata.ecomas.models.AdminRole;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author thomasdwidinata
 */
@Service("AdminRoleServices")
@Transactional
public class AdminRoleServices{
    @Autowired private AdminRoleDao dao;
   
    public void add(AdminRole obj)
    {
        dao.persist(obj);
    }
    
    public List<AdminRole> get()
    {
        return dao.get();
    }
    
    public Object get(int id)
    {
        return dao.get(id);
    }
    
    public void delete(int id)
    {
        dao.delete(id);
    }

    public void update(AdminRole obj) {
        dao.update(obj);
    }
    
    public void get(String arRole)
    {
        dao.get(arRole);
    }
}
