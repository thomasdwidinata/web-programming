package com.thomasdwidinata.ecomas.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.thomasdwidinata.ecomas.dao.*;
import com.thomasdwidinata.ecomas.models.*;
import java.util.List;
import java.util.Map;

/**
 *
 * @author thomasdwidinata
 */
@Service("TransactionStatusSystemServices")
@Transactional
public class TransactionStatusSystemServices {
    
    @Autowired private TransactionHandledDao thd;
    
    @Autowired private TransactionDao tdao;
    
    public List<TransactionHandled> get()
    {
        return thd.get();
    }
    
    public void add(Map<String, Object> data)
    {
        TransactionTable t = tdao.get(Integer.valueOf(data.get("transactionsId").toString()));
        TransactionHandled th = new TransactionHandled();
        
    }
}
