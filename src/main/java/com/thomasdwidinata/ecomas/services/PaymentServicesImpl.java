package com.thomasdwidinata.ecomas.services;

import com.thomasdwidinata.ecomas.models.Payment;
import com.thomasdwidinata.ecomas.dao.PaymentDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author thomasdwidinata
 */
@Service("PaymentServices")
@Transactional
public class PaymentServicesImpl implements PaymentServices {
    
    @Autowired
    private PaymentDao dao;
    
    @Override
    public void add(Payment data)
    {
        dao.add(data);
    }
    
    @Override
    public List<Payment> get(){
        return dao.get();
    }
    @Override
    public void delete(int id){
        dao.delete(id);
    }
    @Override
    public Payment get(int id){
        return dao.get(id);
    }
    @Override
    public void update(Payment tt){
        dao.update(tt);
    }

    @Override
    public Payment get(String name) {
        return dao.get(name);
    }
}
