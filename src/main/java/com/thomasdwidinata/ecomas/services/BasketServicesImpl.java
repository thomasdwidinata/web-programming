package com.thomasdwidinata.ecomas.services;

import com.thomasdwidinata.ecomas.models.Basket;
import com.thomasdwidinata.ecomas.models.BasketItems;
import com.thomasdwidinata.ecomas.models.User;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.thomasdwidinata.ecomas.dao.BasketDao;
import com.thomasdwidinata.ecomas.dao.BasketItemDao;
import com.thomasdwidinata.ecomas.dao.ItemDao;
import com.thomasdwidinata.ecomas.dao.UserDao;

/**
 *
 * @author thomasdwidinata
 */
@Service("BasketServices")
@Transactional
public class BasketServicesImpl implements BasketServices {
    
    @Autowired
    private BasketDao dao;
    
    @Autowired
    private UserDao uDao;
    
    @Autowired
    private ItemDao iDao;
        
    @Autowired
    private BasketItemDao biDao;
    
    @Override
    public void add(String salt, int itemId, int stock){
        User u = uDao.salt(salt);
        if(u == null)
            return;
        Basket b = getLatest(u.getUsername());
        if(b == null)
        {
            b = new Basket();
            b.setUser(u);
        }
        dao.add(b);
        BasketItems bi = new BasketItems();
        bi.setItem(iDao.findById(itemId));
        bi.setTotal(stock);
        bi.setBasket(b);
        biDao.add(bi);
    }
    @Override()
    public List<Basket> get(){
        return dao.get();
    }
    @Override
    public void delete(int id){
        dao.delete(id);
    }
    @Override
    public Basket get(int id){
        return dao.get(id);
    }
    @Override
    public void update(Basket b){
        dao.update(b);
    }
    @Override
    public List<Basket> get(String name){
        User u = uDao.get(name);
        if(u == null)
            return null;
        return dao.getByUser(u);
    }
    @Override
    public int validity(Basket b)
    {
        return dao.validity(b.getBasketId());
    }
    @Override
    public Basket getLatest(String name)
    {
        List<Basket> lb = get(name);
        for(Basket b : lb)
        {
            if(b.isCheckOut() == 0)
            {
                return b;
            }
        }
        return null;
    }
}
