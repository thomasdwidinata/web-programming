package com.thomasdwidinata.ecomas.services;

import java.util.List;
import com.thomasdwidinata.ecomas.models.TransactionTable;
import com.thomasdwidinata.ecomas.models.TransactionsView;
import java.util.Map;

/**
 *
 * @author thomasdwidinata
 */
public interface TransactionServices {
    void add(TransactionTable user);
    void add(Map<String, Object> a);
    List<TransactionTable> get();
    void delete(int id);
    TransactionTable get(int id);
    void update(int id, Map<String,Object> a);
    List<TransactionsView> getBasedOnUser(Map<String, Object> data);
}
