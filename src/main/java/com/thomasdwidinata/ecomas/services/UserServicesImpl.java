package com.thomasdwidinata.ecomas.services;

import com.thomasdwidinata.ecomas.dao.UserDao;
import com.thomasdwidinata.ecomas.models.User;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author thomasdwidinata
 */

@Service("UserServices")
@Transactional
public class UserServicesImpl implements UserServices {
    
    @Autowired
    private UserDao dao;
    
    @Override
    public void add(User user)
    {
        dao.add(user);
    }
    
    @Override
    public List<User> get()
    {
        return dao.get();
    }
    
    @Override
    public void delete(int id)
    {
        dao.delete(id);
    }
    
    @Override
    public User get(int id)
    {
        return dao.get(id);
    }
    
    @Override
    public void update(int id, Map<String, Object> data)
    {
        User u = get(id);
        u.setAddress(data.get("address").toString());
        u.setEmail(data.get("email").toString());
        u.setPassword(data.get("password").toString());
        u.setUsername(data.get("username").toString());
        dao.update(u);
    }
    
    @Override    
    public User get(String username)
    {
        return dao.get(username);
    }
    
    @Override
    public String auth(User user)
    {
        User real = dao.get(user.getUsername());
        if(user.getPassword().equals(real.getPassword()))
        {
            String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder salt = new StringBuilder();
            Random rnd = new Random();
            while (salt.length() < 18) { // length of the random string.
                int index = (int) (rnd.nextFloat() * SALTCHARS.length());
                salt.append(SALTCHARS.charAt(index));
            }
            String saltStr = salt.toString();
            System.out.println("SALT Generated: " + saltStr);
            dao.setToken(real, saltStr);
            return saltStr;
        }
        return null;
    }
    @Override
    public User matchSalt(String salt)
    {
        return dao.salt(salt);
    }
}
