package com.thomasdwidinata.ecomas.services;

import com.thomasdwidinata.ecomas.models.User;
import java.util.List;
import java.util.Map;

/**
 *
 * @author thomasdwidinata
 */
public interface UserServices {
    void add(User user);
    List<User> get();
    void delete(int id);
    User get(int id);
    void update(int id, Map<String, Object> data);
    User get(String username);
    String auth(User user);
    User matchSalt(String salt);
}
