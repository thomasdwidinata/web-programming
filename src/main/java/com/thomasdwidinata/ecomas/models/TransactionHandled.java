package com.thomasdwidinata.ecomas.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author thomasdwidinata
 */
@Entity
@Table(name="transactionhandled")
public class TransactionHandled implements Serializable {
    
    @Id
    @ManyToOne(optional = false)
    @JoinColumn(name = "transactionId", nullable = false)
    private TransactionTable transactionId;

    public TransactionTable getTransactionsId() {
        return transactionId;
    }

    public void setTransactionsId(TransactionTable transactionsId) {
        this.transactionId = transactionsId;
    }
    
    @Id
    @ManyToOne(optional = false)
    @JoinColumn(name = "transactionStatusId", nullable = false)
    private TransactionStatus transactionStatusId;
    
    @Column(name = "timestamp", nullable = true)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date timestamp;

    public TransactionStatus getTransactionStatusId() {
        return transactionStatusId;
    }

    public void setTransactionStatusId(TransactionStatus transactionStatusId) {
        this.transactionStatusId = transactionStatusId;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
}
