package com.thomasdwidinata.ecomas.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author thomasdwidinata
 */
@Entity
@Table(name = "shipping")
public class Shipping implements Serializable{
    @Id
    @Column(name = "shippingId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int shippingId;

    @Column(name="shippingName", unique = true, nullable = false)
    private String shippingName;

    public int getShippingId() {
        return shippingId;
    }

    public void setShippingId(int shippingId) {
        this.shippingId = shippingId;
    }

    public String getShippingName() {
        return shippingName;
    }

    public void setShippingName(String shippingName) {
        this.shippingName = shippingName;
    }
}
