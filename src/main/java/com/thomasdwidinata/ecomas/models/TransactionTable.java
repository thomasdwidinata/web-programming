package com.thomasdwidinata.ecomas.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author thomasdwidinata
 */
@Entity
@Table(name = "transaction")
public class TransactionTable implements Serializable{
    
    @Id
    @Column(name = "transactionId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int transactionId;
    
    @ManyToOne(optional = false)
    @JoinColumn(name = "basketId", nullable = false)
    private Basket basket;
    
    @ManyToOne
    @JoinColumn(name = "shippingId", nullable = false)
    private Shipping shipping;
    
    @ManyToOne
    @JoinColumn(name = "paymentId", nullable = false)
    private Payment paymentId;

    public Basket getBasket() {
        return basket;
    }

    public void setBasket(Basket basket) {
        this.basket = basket;
    }

    @Column(name = "paymentInfo", nullable = true)
    private String paymentInfo;
    
    @Column(name = "address", nullable = true)
    private String address;

    public Payment getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Payment paymentId) {
        this.paymentId = paymentId;
    }

    public String getPaymentInfo() {
        return paymentInfo;
    }

    public void setPaymentInfo(String paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getTransactionHistoryId() {
        return transactionId;
    }

    public void setTransactionHistoryId(int transactionHistoryId) {
        this.transactionId = transactionHistoryId;
    }


    public Shipping getShipping() {
        return shipping;
    }

    public void setShipping(Shipping shipping) {
        this.shipping = shipping;
    }
}
