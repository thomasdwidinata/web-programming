package com.thomasdwidinata.ecomas.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author thomasdwidinata
 */
@Entity
@Table(name = "basket")
public class Basket implements Serializable{
    @Id
    @Column(name = "basketId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int basketId;
    
    @ManyToOne
    @JoinColumn(name="basketOwner")
    private User basketOwner;
    
    @Column(name = "checkedOut")
    private int checkedOut;
    public int isCheckOut() {
        return checkedOut;
    }

    public void setCheckOut(int checkOut) {
        this.checkedOut = checkOut;
    }

    public int getBasketId() {
        return basketId;
    }

    public void setBasketId(int basketId) {
        this.basketId = basketId;
    }

    public User getUser() {
        return basketOwner;
    }

    public void setUser(User user) {
        this.basketOwner = user;
    }
    
}
