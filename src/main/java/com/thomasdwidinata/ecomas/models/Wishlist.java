package com.thomasdwidinata.ecomas.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author thomasdwidinata
 */
@Entity
@Table(name = "wishlist")
public class Wishlist implements Serializable{

    @Id
    @ManyToOne
    @JoinColumn(name = "itemId", nullable = false)
    private Item item;
    
    @Id
    @ManyToOne
    @JoinColumn(name = "userId", nullable = false)
    private User user;

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
}
