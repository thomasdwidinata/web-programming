package com.thomasdwidinata.ecomas.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author thomasdwidinata
 */
@Entity
@Table(name="notification")
public class Notification implements Serializable {
    @Id
    @Column(name = "notificationId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int notificationId;

    @ManyToOne(optional = false)
    @JoinColumn(name="sender", nullable = true)
    private User sender;
    
    @ManyToOne(optional = false)
    @JoinColumn(name = "receiver", nullable = true)
    private User receiver;
    
    @ManyToOne(optional = false)
    @JoinColumn(name="adminSender", nullable = true)
    private User adminSender;

    public User getAdminSender() {
        return adminSender;
    }

    public void setAdminSender(User adminSender) {
        this.adminSender = adminSender;
    }

    public User getAdminReceiver() {
        return adminReceiver;
    }

    public void setAdminReceiver(User adminReceiver) {
        this.adminReceiver = adminReceiver;
    }
    
    @ManyToOne(optional = false)
    @JoinColumn(name = "adminReceiver", nullable = true)
    private User adminReceiver;
    
    @Column(name = "subject", nullable = false)
    private String subject;
    
    @Column(name = "message", nullable = false)
    private String message;
    
    @Column(name = "notificationRead", nullable = false)
    private int notificationRead;
    
    @Column(name = "notificationType", nullable = false)
    private int type; // 1 for normal notification, 2 for negotiation

    public int getNotificationRead() {
        return notificationRead;
    }

    public void setNotificationRead(int notificationRead) {
        this.notificationRead = notificationRead;
    }

    public int getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(int notificationId) {
        this.notificationId = notificationId;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
