package com.thomasdwidinata.ecomas.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author thomasdwidinata
 */
@Entity
@Table(name = "basketitems")
public class BasketItems implements Serializable {
    @Id
    @Column(name = "basketItemList")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int basketItemList;
    
    @ManyToOne
    @JoinColumn(name = "basketId")
    private Basket basketId;
    
    @ManyToOne
    @JoinColumn(name = "itemId")
    private Item itemId;
    
    @Column(name = "takenItem")
    private int takenItem;

    public int getTransactionHistoryId() {
        return basketItemList;
    }

    public void setTransactionHistoryId(int transactionHistoryId) {
        this.basketItemList = transactionHistoryId;
    }

    public Basket getBasket() {
        return basketId;
    }

    public void setBasket(Basket basket) {
        this.basketId = basket;
    }

    public Item getItem() {
        return itemId;
    }

    public void setItem(Item item) {
        this.itemId = item;
    }

    public int getTotal() {
        return takenItem;
    }

    public void setTotal(int total) {
        this.takenItem = total;
    }
}
