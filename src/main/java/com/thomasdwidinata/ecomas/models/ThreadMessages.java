package com.thomasdwidinata.ecomas.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author thomasdwidinata
 */
@Entity
@Table(name = "threadmessages")
public class ThreadMessages implements Serializable{
    @Id
    @Column(name = "messageId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int messageId;

    @ManyToOne(optional = false)
    @JoinColumn(name="userId", nullable = true)
    private User user;
    
    @ManyToOne(optional = false)
    @JoinColumn(name="adminId", nullable = true)
    private Admin admin;
    
    @ManyToOne(optional = false)
    @JoinColumn(name = "threadId", nullable = false)
    private ThreadTitle thread;
    
    @Column(name = "messageContent")
    private String messageContent;

    public Admin getAdmin() {
        return admin;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ThreadTitle getThread() {
        return thread;
    }

    public void setThread(ThreadTitle thread) {
        this.thread = thread;
    }
    
    
}
