package com.thomasdwidinata.ecomas.models;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author thomasdwidinata
 */
@Entity
@Table(name="basketview")
public class BasketView implements Serializable{
    @Id
    @Column(name="basketId")
    private int basketId;
    
    @Column(name="username")
    private String username;
    
    @Column(name = "itemName")
    private String itemName;
    
    @Column(name = "takenItem")
    private int takenItem;
    
    @Column(name = "checkedOut")
    private boolean checkedOut;

    public int getBasketId() {
        return basketId;
    }

    public void setBasketId(int basketId) {
        this.basketId = basketId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getTakenItem() {
        return takenItem;
    }

    public void setTakenItem(int takenItem) {
        this.takenItem = takenItem;
    }

    public boolean getCheckedOut() {
        return checkedOut;
    }

    public void setCheckedOut(boolean checkedOut) {
        this.checkedOut = checkedOut;
    }
    
    
}
