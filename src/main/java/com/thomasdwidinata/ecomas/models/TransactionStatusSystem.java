package com.thomasdwidinata.ecomas.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author thomasdwidinata
 */
@Entity
@Table(name = "transactionstatussystem")
public class TransactionStatusSystem implements Serializable {
    @Id
    @Column(name = "transactionId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int transactionstatussystemId;
    
    @Column(name = "transactionStatusName")
    private String transactionStatusName;
    
    @Column(name = "timestamp", nullable = true)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date timestamp;

    public int getTransactionstatussystemId() {
        return transactionstatussystemId;
    }

    public void setTransactionstatussystemId(int transactionstatussystemId) {
        this.transactionstatussystemId = transactionstatussystemId;
    }

    public String getTransactionStatusName() {
        return transactionStatusName;
    }

    public void setTransactionStatusName(String transactionStatusName) {
        this.transactionStatusName = transactionStatusName;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
}
