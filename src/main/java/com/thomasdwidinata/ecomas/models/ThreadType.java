package com.thomasdwidinata.ecomas.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author thomasdwidinata
 */
@Entity
@Table(name = "threadtype")
public class ThreadType implements Serializable{
    
    @Id
    @Column(name = "threadTypeId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int threadTypeId;
    
    @Column(name = "threadTypeName", nullable = false)
    private String threadTypeName;

    public int getThreadTypeId() {
        return threadTypeId;
    }

    public void setThreadTypeId(int threadTypeId) {
        this.threadTypeId = threadTypeId;
    }

    public String getThreadTypeName() {
        return threadTypeName;
    }

    public void setThreadTypeName(String threadTypeName) {
        this.threadTypeName = threadTypeName;
    }
    
}
