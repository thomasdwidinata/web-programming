package com.thomasdwidinata.ecomas.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author thomasdwidinata
 */
@Entity
@Table(name="itemuom")
public class ItemUom implements Serializable{
    
    @Id
    @Column(name = "uomId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int uomId;

    @ManyToOne(optional = false)
    @JoinColumn(name="itemId", nullable = false)
    private Item item;
    
    @ManyToOne(optional = false)
    @JoinColumn(name="unitId", nullable = false)
    private Unit unit;
    
    @Column(name = "qty")
    private int qty;

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }
    
    @Column(name = "price")
    private double price;

    public int getUomId() {
        return uomId;
    }

    public void setUomId(int uomId) {
        this.uomId = uomId;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    
    
}
