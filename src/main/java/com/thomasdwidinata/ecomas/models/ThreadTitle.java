package com.thomasdwidinata.ecomas.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author thomasdwidinata
 */
@Entity
@Table(name = "threadtitle")
public class ThreadTitle implements Serializable{
    
    @Id
    @Column(name = "threadId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int threadId;
    
    @Column(name = "threadName", nullable = false)
    private String threadName;
    
    @ManyToOne(optional = false)
    @JoinColumn(name = "threadTypeId", nullable = false)
    private ThreadType threadTypeId;
    
    public int getThreadId() {
        return threadId;
    }

    public void setThreadId(int threadId) {
        this.threadId = threadId;
    }

    public String getThreadName() {
        return threadName;
    }

    public void setThreadName(String threadName) {
        this.threadName = threadName;
    }

    public ThreadType getThreadTypeId() {
        return threadTypeId;
    }

    public void setThreadTypeId(ThreadType threadTypeId) {
        this.threadTypeId = threadTypeId;
    }

}
