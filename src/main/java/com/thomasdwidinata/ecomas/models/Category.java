package com.thomasdwidinata.ecomas.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table; 

/**
 *
 * @author thomasdwidinata
 */
@Entity
@Table(name="category")
public class Category implements Serializable{
    
    @Id
    @Column(name = "categoryId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int catId;
    
    @Column(name = "categoryName", unique = true, nullable = false)
    private String categoryName;
    
    public int getId() {
        return catId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setId(int id) {
        this.catId = id;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
