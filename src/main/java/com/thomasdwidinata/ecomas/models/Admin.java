package com.thomasdwidinata.ecomas.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author thomasdwidinata
 */
@Entity
@Table(name="admin")
public class Admin implements Serializable{

    public int getAdminId() {
        return adminId;
    }

    public void setAdminId(int adminId) {
        this.adminId = adminId;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public AdminRole getRoleId() {
        return roleId;
    }

    public void setRoleId(AdminRole roleId) {
        this.roleId = roleId;
    }
    
    @Id
    @Column(name = "adminId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int adminId;
    
    @Column(name = "adminName", nullable = true)
    private String adminName;
    
    @Column(name = "password", nullable = true)
    private String password;
    
    @Column(name = "salt", nullable = true)
    private String salt;
    
    @ManyToOne(optional = false)
    @JoinColumn(name="roleId", nullable = true)
    private AdminRole roleId;
    
    
}
