package com.thomasdwidinata.ecomas.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author thomasdwidinata
 */
@Entity
@Table(name="item")
public class Item implements Serializable{
    
    @Id
    @Column(name = "itemId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int itemId;

    @ManyToOne(optional = false)
    @JoinColumn(name="categoryId", nullable = false)
    private Category category;
    
    @Column(name="itemName", nullable = false)
    private String itemName;
    
    @ManyToOne
    @JoinColumn(name="seller", nullable = false)
    private User seller;

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public User getSeller() {
        return seller;
    }

    public void setSeller(User seller) {
        this.seller = seller;
    }
    
    
    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getId() {
        return itemId;
    }

    public void setId(int id) {
        this.itemId = id;
    }

    public String getItemName() {
        return itemName;
    }
    
    public String toString(){
        return "ID : " + this.itemId + "; Name : " + this.itemName + "; category : " + this.category.getCategoryName() ;
    }
    
}
