package com.thomasdwidinata.ecomas.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author thomasdwidinata
 */
@Entity
@Table(name = "threadpermission")
public class ThreadPermission implements Serializable{
    @Id
    @Column(name = "threadPermissionId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int threadPermissionId;

    @ManyToOne(optional = false)
    @JoinColumn(name="userId", nullable = false)
    private User user;
    
    @ManyToOne(optional = false)
    @JoinColumn(name="threadId", nullable = false)
    private ThreadTitle threadId;

    public int getThreadPermissionId() {
        return threadPermissionId;
    }

    public void setThreadPermissionId(int threadPermissionId) {
        this.threadPermissionId = threadPermissionId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ThreadTitle getThreadId() {
        return threadId;
    }

    public void setThreadId(ThreadTitle threadId) {
        this.threadId = threadId;
    }
    
}
