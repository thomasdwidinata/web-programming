package com.thomasdwidinata.ecomas.dao;

import java.util.List;
import com.thomasdwidinata.ecomas.models.AdminRole;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author thomasdwidinata
 */
@Repository("AdminRoleDao")
public class AdminRoleDao extends AbstractDao{
    
    @Transactional
    public void add(AdminRole obj)
    {
        persist(obj);
    }
    
    public List<AdminRole> get()
    {
        Criteria c = getSession().createCriteria(AdminRole.class);
        return c.list();
    }
    
    public AdminRole get(int id)
    {
        Criteria c = getSession().createCriteria(AdminRole.class);
        c.add(Restrictions.eq("roleId", id));
        return (AdminRole) c.uniqueResult();
    }
    
    public AdminRole get(String arRole)
    {
        Criteria c = getSession().createCriteria(AdminRole.class);
        c.add(Restrictions.eq("roleName", arRole));
        return (AdminRole) c.uniqueResult();
    }
    
    public void update(AdminRole obj)
    {
        getSession().update(obj);
    }
    
    public void delete(int id)
    {
        AdminRole ar = new AdminRole();
        ar.setRoleId(id);
        getSession().delete(ar);
    }
    
    public void delete(AdminRole obj){
        getSession().delete(obj);
    }
}
