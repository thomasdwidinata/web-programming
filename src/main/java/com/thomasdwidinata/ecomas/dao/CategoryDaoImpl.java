package com.thomasdwidinata.ecomas.dao;

import com.thomasdwidinata.ecomas.models.Category;
import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author thomasdwidinata
 */
@Repository("CategoryDao")
public class CategoryDaoImpl extends AbstractDao implements CategoryDao {
    @Override
    @Transactional
    public void addCategory(Category cat)
    {
        persist(cat);
    }
    
    @Override
    public List<Category> findAllCategory(){
        Criteria c = getSession().createCriteria(Category.class);
        return (List<Category>) c.list();
    }
    
    @Override
    @Transactional
    public void deleteCategoryById(int id){
        Query q = getSession().createSQLQuery("DELETE FROM `category` WHERE `categoryId` = :id");
        q.setString("id", String.valueOf(id));
        q.executeUpdate();
    }
    
    @Override
    public Category findById(int id){
        Criteria c = getSession().createCriteria(Category.class);
        c.add(Restrictions.eq("id", id));
        return (Category) c.uniqueResult();
    }
    
    @Override
    @Transactional
    public void updateCategory(Category cat){
        getSession().update(cat);
    }
    
    @Override
    public Category findCategoryByName(String name){
        Criteria c = getSession().createCriteria(Category.class);
        c.add(Restrictions.eq("categoryName", name));
        return (Category) c.uniqueResult();
    }
}
