package com.thomasdwidinata.ecomas.dao;

import java.util.List;
import com.thomasdwidinata.ecomas.models.User;

/**
 *
 * @author thomasdwidinata
 */
public interface UserDao {
    void add(User user);
    List<User> get();
    void delete(int id);
    User get(int id);
    void update(User user);
    User get(String username);
    void setToken(User user, String token);
    void revokeToken(User user);
    User salt(String salt);
}
