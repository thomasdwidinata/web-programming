package com.thomasdwidinata.ecomas.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.thomasdwidinata.ecomas.models.*;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author thomasdwidinata
 */
@Repository("ThreadPermissionDao")
public class ThreadPermissionDao extends AbstractDao{
    @Transactional
    public void add(ThreadPermissionDao tp)
    {
        persist(tp);
    }
    
    @Transactional
    public void delete(int id)
    {
        Criteria c = getSession().createCriteria(ThreadPermissionDao.class);
        c.add(Restrictions.eq("threadPermissionId", id));
        getSession().delete((ThreadPermissionDao) c.uniqueResult());
    }
    
    
}
