package com.thomasdwidinata.ecomas.dao;

import com.thomasdwidinata.ecomas.models.Item;
import com.thomasdwidinata.ecomas.models.ItemList;
import java.util.List;

/**
 *
 * @author thomasdwidinata
 */
public interface ItemListDao {
    List<ItemList> get();
    ItemList get(int id);
    List<ItemList> get(String name);
    List<ItemList> getMySoldItems(String name);
}
