package com.thomasdwidinata.ecomas.dao;

import java.util.List;
import com.thomasdwidinata.ecomas.models.Admin;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


/**
 *
 * @author thomasdwidinata
 */
@Repository("AdminDao")
public class AdminDao extends AbstractDao{

    @Transactional
    public void add(Admin obj) {
        persist(obj);
    }

    public List<Admin> get() {
        Criteria a = getSession().createCriteria(Admin.class);
        return a.list();
    }

    public Admin get(int id)
    {
        Criteria c = getSession().createCriteria(Admin.class);
        c.add(Restrictions.eq("id", id));
        return (Admin) c.uniqueResult();
    }
    
    @Transactional
    public void update(Admin obj) {
        getSession().update(obj);
    }
   
    @Transactional
    public void delete(int id)
    {
        Admin a = new Admin();
        a.setAdminId(id);
        getSession().delete(a);
    }
    
    public void delete(Admin obj)
    {
        getSession().delete(obj);
    }
}
