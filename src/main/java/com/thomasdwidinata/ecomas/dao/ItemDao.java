package com.thomasdwidinata.ecomas.dao;

import com.thomasdwidinata.ecomas.models.Item;
import java.util.List;

/**
 *
 * @author thomasdwidinata
 */
public interface ItemDao {
    void addItem(Item item);
    List<Item> findAllItem();
    void deleteItemById(int id);
    Item findById(int id);
    void updateItem(Item item);
    List<Item> findByName(String item);
    Item findByNameSpecific(String item);
    
//    Item findByNameWithCatName(String item, String cat);
}
