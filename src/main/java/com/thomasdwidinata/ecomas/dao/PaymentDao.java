package com.thomasdwidinata.ecomas.dao;

import com.thomasdwidinata.ecomas.models.Payment;
import java.util.List;

/**
 *
 * @author thomasdwidinata
 */
public interface PaymentDao {
    void add(Payment data);
    List<Payment> get();
    void delete(int id);
    Payment get(int id);
    void update(Payment data);
    Payment get(String name);
}
