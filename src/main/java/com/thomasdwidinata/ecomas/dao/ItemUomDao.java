package com.thomasdwidinata.ecomas.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.thomasdwidinata.ecomas.models.*;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author thomasdwidinata
 */
@Repository("ItemUomDao")
public class ItemUomDao extends AbstractDao {
    @Transactional
    public void add(ItemUom i)
    {
        persist(i);
    }
    
    public List<ItemUom> get()
    {
        Criteria c = getSession().createCriteria(ItemUom.class);
        return c.list();
    }
    
    public List<ItemUom> get(int itemId)
    {
        Criteria c = getSession().createCriteria(ItemUom.class);
        c.add(Restrictions.eq("itemId", itemId));
        return (List<ItemUom>)  c.list();
    }
    
    public void update(ItemUom i )
    {
        getSession().update(i);
    }
    
    
}
