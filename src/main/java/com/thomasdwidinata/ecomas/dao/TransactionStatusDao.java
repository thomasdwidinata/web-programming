package com.thomasdwidinata.ecomas.dao;

import org.springframework.stereotype.Repository;
import com.thomasdwidinata.ecomas.models.*;
import java.util.List;
import java.util.Map;
import org.hibernate.Criteria;

/**
 *
 * @author thomasdwidinata
 */
@Repository("TransactionStatusDao")
public class TransactionStatusDao extends AbstractDao {
    public List<TransactionStatus> get()
    {
        Criteria c = getSession().createCriteria(TransactionStatus.class);
        return (List<TransactionStatus>) c.list();
    }
    
    
}
