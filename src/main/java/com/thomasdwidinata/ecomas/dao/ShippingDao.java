package com.thomasdwidinata.ecomas.dao;

import com.thomasdwidinata.ecomas.models.Shipping;
import java.util.List;

/**
 *
 * @author thomasdwidinata
 */
public interface ShippingDao {
    void add(Shipping tt);
    List<Shipping> get();
    void delete(int id);
    Shipping get(int id);
    void update(Shipping tt);
    Shipping get(String name);
}
