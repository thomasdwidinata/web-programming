package com.thomasdwidinata.ecomas.dao;

import com.thomasdwidinata.ecomas.models.Category;
import java.util.List;

/**
 *
 * @author thomasdwidinata
 */
public interface CategoryDao {
    void addCategory(Category cat);
    List<Category> findAllCategory();
    void deleteCategoryById(int id);
    Category findById(int id);
    void updateCategory(Category cat);
    Category findCategoryByName(String name);
}
