package com.thomasdwidinata.ecomas.dao;

import com.thomasdwidinata.ecomas.models.Basket;
import com.thomasdwidinata.ecomas.models.User;
import java.util.List;

/**
 *
 * @author thomasdwidinata
 */
public interface BasketDao {
    void add(Basket tt);
    List<Basket> get();
    void delete(int id);
    Basket get(int id);
    void update(Basket tt);
    List<Basket> getByUser(User u);
    int validity(int id);
}
