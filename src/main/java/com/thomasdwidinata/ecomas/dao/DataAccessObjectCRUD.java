/*
This is an interface for DAOs that implements Basic CRUD
*/

package com.thomasdwidinata.ecomas.dao;

import java.util.List;

/**
 *
 * @author thomasdwidinata
 */
public interface DataAccessObjectCRUD {
    void add(Object obj);
    List<Object> get();
    void update(Object obj);
    void delete(int id);
}
