package com.thomasdwidinata.ecomas.dao;

import com.thomasdwidinata.ecomas.models.BasketItems;
import java.util.List;

/**
 *
 * @author thomasdwidinata
 */
public interface BasketItemDao {
    void add(BasketItems bi);
    void update(BasketItems bi);
    void delete(int id);
    List<BasketItems> get();
    List<BasketItems> get(int id);
}
