package com.thomasdwidinata.ecomas.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.thomasdwidinata.ecomas.models.*;
import java.util.List;
import org.hibernate.Criteria;
/**
 *
 * @author thomasdwidinata
 */
@Repository("TransactionHandledDao")
public class TransactionHandledDao extends AbstractDao{
    @Transactional
    public void add(TransactionHandled th)
    {
        persist(th);
    }
    
    public List<TransactionHandled> get()
    {
        Criteria c = getSession().createCriteria(TransactionHandled.class);
        return (List<TransactionHandled>) c.list();
    }
    
}
