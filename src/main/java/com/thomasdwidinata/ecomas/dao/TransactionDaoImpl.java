package com.thomasdwidinata.ecomas.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.thomasdwidinata.ecomas.models.TransactionTable;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author thomasdwidinata
 */
@Repository("TransactionalDao")
public class TransactionDaoImpl extends AbstractDao implements TransactionDao{
    @Override
    @Transactional
    public void add(TransactionTable tt)
    {
        persist(tt);
    }
    
    @Override
    public List<TransactionTable> get()
    {
        Criteria c = getSession().createCriteria(TransactionTable.class);
        return (List<TransactionTable>) c.list();
    }
    
    @Override
    @Transactional
    public void delete(int id)
    {
        Criteria c = getSession().createCriteria(TransactionTable.class);
        c.add(Restrictions.eq("transactionId", id));
        delete((TransactionTable) c.uniqueResult());
    }
    
    @Override
    public TransactionTable get(int userId)
    {
        Criteria c = getSession().createCriteria(TransactionTable.class);
        c.add(Restrictions.eq("transactionId", userId));
        return (TransactionTable) c.uniqueResult();
    }
    
    @Override
    @Transactional
    public void update(TransactionTable user)
    {
        getSession().update(user);
    }
   
}
