package com.thomasdwidinata.ecomas.dao;

import com.thomasdwidinata.ecomas.models.*;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 *
 * @author thomasdwidinata
 */
@Repository("ThreadMessageDao")
public class ThreadMessageDao extends AbstractDao{
    public List<ThreadMessages> get(){
        Criteria c = getSession().createCriteria(ThreadMessages.class);
        return (List<ThreadMessages>) c.list();
    }
    public List<ThreadMessages> get(int conversation){
        Criteria c = getSession().createCriteria(ThreadMessages.class);
        c.add(Restrictions.eq("threadId", conversation));
        return (List<ThreadMessages>) c.list();
    }
    public void add(ThreadMessages tm)
    {
        persist(tm);
    }
}
