package com.thomasdwidinata.ecomas.dao;

import com.thomasdwidinata.ecomas.models.Item;
import com.thomasdwidinata.ecomas.models.ItemList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 *
 * @author thomasdwidinata
 */
@Repository("ItemListDao")
public class ItemListDaoImpl extends AbstractDao implements ItemListDao {
    @Override
    public List<ItemList> get(){
        Criteria c = getSession().createCriteria(ItemList.class);
        return (List<ItemList>) c.list();
    }
    
    @Override
    public List<ItemList> getMySoldItems(String name)
    {
        Criteria c = getSession().createCriteria(ItemList.class);
        c.add(Restrictions.eq("username", name));
        return (List<ItemList>) c.list();
    }
    @Override
    public ItemList get(int id){
        Criteria c = getSession().createCriteria(ItemList.class);
        c.add(Restrictions.eq("itemId", id));
        return (ItemList) c.uniqueResult();
    }
    @Override
    public List<ItemList> get(String name){
        Criteria c = getSession().createCriteria(ItemList.class);
        c.add(Restrictions.like("itemName", "%"+name+"%"));
        return (List<ItemList>) c.list();
    }
}
