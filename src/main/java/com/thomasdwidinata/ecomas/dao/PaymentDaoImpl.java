package com.thomasdwidinata.ecomas.dao;

import org.springframework.stereotype.Repository;
import com.thomasdwidinata.ecomas.models.Payment;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author thomasdwidinata
 */
@Repository("PaymentDao")
public class PaymentDaoImpl extends AbstractDao implements PaymentDao{
    @Override
    @Transactional
    public void add(Payment tt)
    {
        persist(tt);
    }
    
    @Override
    public List<Payment> get()
    {
        Criteria c = getSession().createCriteria(Payment.class);
        return (List<Payment>) c.list();
    }
    
    @Override
    @Transactional
    public void delete(int id)
    {
        Criteria c = getSession().createCriteria(Payment.class);
        c.add(Restrictions.eq("paymentId", id));
        delete((Payment) c.uniqueResult());
    }
    
    @Override
    public Payment get(int userId)
    {
        Criteria c = getSession().createCriteria(Payment.class);
        c.add(Restrictions.eq("paymentId", userId));
        return (Payment) c.uniqueResult();
    }
    
    @Override
    @Transactional
    public void update(Payment data)
    {
        getSession().update(data);
    }
    
    @Override
    public Payment get(String name)
    {
        Criteria c = getSession().createCriteria(Payment.class);
        c.add(Restrictions.eq("paymentName", name));
        return (Payment) c.uniqueResult();
    }
}
