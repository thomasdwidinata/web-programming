
package com.thomasdwidinata.ecomas.dao;

import org.springframework.stereotype.Repository;
import com.thomasdwidinata.ecomas.models.*;
import com.thomasdwidinata.ecomas.dao.*;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author thomasdwidinata
 */
@Repository("TransactionViewDao")
public class TransactionViewDao extends AbstractDao {
    public List<TransactionsView> get()
    {
        Criteria c = getSession().createCriteria(TransactionsView.class);
        return (List<TransactionsView>) c.list();
    }
    
    public List<TransactionsView> get(String name)
    {
        Criteria c = getSession().createCriteria(TransactionTable.class);
        c.add(Restrictions.eq("userName", name));
        return (List<TransactionsView>)c.list();
    }
}
