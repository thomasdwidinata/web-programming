package com.thomasdwidinata.ecomas.dao;

import com.thomasdwidinata.ecomas.models.Unit;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 *
 * @author thomasdwidinata
 */
@Repository("UnitDao")
public class UnitDao extends AbstractDao {
    public List<Unit> get()
    {
        Criteria c = getSession().createCriteria(Unit.class);
        return (List<Unit>) c.list();
    }
    
    public Unit get(String name)
    {
        Criteria c = getSession().createCriteria(Unit.class);
        c.add(Restrictions.eq("unitName", name));
        return (Unit) c.uniqueResult();
    }
    
    public void add(Unit u)
    {
        persist(u);
    }
    
    public void update(Unit u)
    {
        getSession().update(u);
    }
    
    public void delete(Unit u)
    {
        getSession().delete(u);
    }
}
