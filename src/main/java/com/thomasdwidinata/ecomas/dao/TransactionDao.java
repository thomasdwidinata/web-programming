package com.thomasdwidinata.ecomas.dao;

import com.thomasdwidinata.ecomas.models.TransactionTable;
import com.thomasdwidinata.ecomas.models.TransactionsView;
import java.util.List;

/**
 * 
* @author thomasdwidinata
 */
public interface TransactionDao {
    void add(TransactionTable tt);
    List<TransactionTable> get();
    void delete(int id);
    TransactionTable get(int id);
    void update(TransactionTable tt);
}
