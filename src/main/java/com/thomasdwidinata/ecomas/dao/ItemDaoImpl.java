package com.thomasdwidinata.ecomas.dao;

import com.thomasdwidinata.ecomas.models.Item;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author thomasdwidinata
 */
@Repository("ItemDao")
public class ItemDaoImpl extends AbstractDao implements ItemDao{
    @Override
    @Transactional
    public void addItem(Item item)
    {
        persist(item);
    }
    
    @Override
    public Item findByNameSpecific(String item)
    {
        Criteria c = getSession().createCriteria(Item.class);
        c.add(Restrictions.like("itemName", item));
        return (Item) c.uniqueResult();
    }
    
    @Override
    public List<Item> findAllItem()
    {
        Criteria c = getSession().createCriteria(Item.class);
        return (List<Item>) c.list();
    }
    
    @Override
    @Transactional
    public void deleteItemById(int id)
    {
        Query q = getSession().createSQLQuery("DELETE FROM `item` WHERE `itemId` = :id");
        q.setString("id", String.valueOf(id));
        q.executeUpdate();
    }
    
    @Override
    public Item findById(int id)
    {
        Criteria c = getSession().createCriteria(Item.class);
        c.add(Restrictions.eq("id", id));
        return (Item) c.uniqueResult();
    }
    
    @Override
    @Transactional
    public void updateItem(Item item)
    {
        getSession().update(item);
    }
    
    @Override
    public List<Item> findByName(String item){
        Criteria c = getSession().createCriteria(Item.class);
        c.add(Restrictions.like("itemName", "%"+item+"%"));
        return (List<Item>) c.list();
    }
}
