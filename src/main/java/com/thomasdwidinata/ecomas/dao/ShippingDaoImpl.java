package com.thomasdwidinata.ecomas.dao;

import org.springframework.stereotype.Repository;
import com.thomasdwidinata.ecomas.models.Shipping;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author thomasdwidinata
 */
@Repository("ShippingDao")
public class ShippingDaoImpl extends AbstractDao implements ShippingDao{
    @Override
    @Transactional
    public void add(Shipping tt)
    {
        persist(tt);
    }
    
    @Override
    public List<Shipping> get()
    {
        Criteria c = getSession().createCriteria(Shipping.class);
        return (List<Shipping>) c.list();
    }
    
    @Override
    @Transactional
    public void delete(int id)
    {
        Criteria c = getSession().createCriteria(Shipping.class);
        c.add(Restrictions.eq("shipping", id));
//        return (TransactionTable) c.uniqueResult();
        getSession().delete((Shipping) c.uniqueResult());
    }
    
    @Override
    public Shipping get(int userId)
    {
        Criteria c = getSession().createCriteria(Shipping.class);
        c.add(Restrictions.eq("shippingId", userId));
        return (Shipping) c.uniqueResult();
    }
    
    @Override
    public Shipping get(String name)
    {
        Criteria c = getSession().createCriteria(Shipping.class);
        c.add(Restrictions.eq("shippingName", name));
        return (Shipping) c.uniqueResult();
    }
    
    @Override
    @Transactional
    public void update(Shipping user)
    {
        getSession().update(user);
    }
}
