package com.thomasdwidinata.ecomas.dao;

import org.springframework.stereotype.Repository;
import com.thomasdwidinata.ecomas.models.*;
import java.util.List;

/**
 *
 * @author thomasdwidinata
 */
@Repository("ThreadTypeDao")
public class ThreadTypeDao extends AbstractDao{
    public void add(ThreadType tt)
    {
        persist(tt);
    }
//    public List<ThreadType> get()
//    {
//        return 
//    }
}
