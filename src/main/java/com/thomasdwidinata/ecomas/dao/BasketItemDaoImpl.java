package com.thomasdwidinata.ecomas.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.thomasdwidinata.ecomas.models.BasketItems;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author thomasdwidinata
 */
@Repository("BasketItemsDao")
public class BasketItemDaoImpl extends AbstractDao implements BasketItemDao{
    
    @Override
    @Transactional
    public void add(BasketItems bi)
    {
        persist(bi);
    }
    @Override
    public List<BasketItems> get()
    {
        Criteria c = getSession().createCriteria(BasketItems.class);
        return (List<BasketItems>) c.list();
    }
    @Override
    public void delete(int id)
    {
        delete(id);
    }
    @Override
    public void update(BasketItems bi)
    {
        getSession().update(bi);
    }
    @Override
    public List<BasketItems> get(int id)
    {
        Criteria c = getSession().createCriteria(BasketItems.class);
        c.add(Restrictions.eq("basketId", id));
        return c.list();
    }
}
