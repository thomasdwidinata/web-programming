package com.thomasdwidinata.ecomas.dao;

import java.util.List;
import org.springframework.stereotype.Repository;
import com.thomasdwidinata.ecomas.dao.*;
import com.thomasdwidinata.ecomas.models.*;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author thomasdwidinata
 */
@Repository("ThreadTitleDao")
public class ThreadTitleDao extends AbstractDao{
    public List<ThreadTitle> get(){
        Criteria c = getSession().createCriteria(ThreadTitle.class);
        return c.list();
    }
    
    public List<ThreadTitle> get(int id)
    {
        Criteria c = getSession().createCriteria(ThreadTitle.class);
        c.add(Restrictions.eq("threadId", id));
        return (List<ThreadTitle>) c.list();
    }
    
    public List<ThreadTitle> get(String title)
    {
        Criteria c = getSession().createCriteria(ThreadTitle.class);
        c.add(Restrictions.eq("threadName", title));
        return (List<ThreadTitle>) c.list();
    }
    
    public void add(ThreadTitle tt)
    {
        persist(tt);
    }
}
