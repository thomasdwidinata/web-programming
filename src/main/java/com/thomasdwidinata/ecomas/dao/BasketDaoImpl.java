package com.thomasdwidinata.ecomas.dao;

import java.util.List;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.thomasdwidinata.ecomas.models.Basket;
import com.thomasdwidinata.ecomas.models.User;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author thomasdwidinata
 */
@Repository("BasketDao")
public class BasketDaoImpl extends AbstractDao implements BasketDao {
    
    @Override
    @Transactional
    public void add(Basket tt)
    {
        persist(tt);
    }
    
    @Override
    public List<Basket> get()
    {
        Criteria c = getSession().createCriteria(Basket.class);
        return (List<Basket>) c.list();
    }
    
    @Override
    @Transactional
    public void delete(int id)
    {
        Criteria c = getSession().createCriteria(Basket.class);
        c.add(Restrictions.eq("basketId", id));
//        return (TransactionTable) c.uniqueResult();
        delete((Basket) c.uniqueResult());
    }
    
    @Override
    public Basket get(int userId)
    {
        Criteria c = getSession().createCriteria(Basket.class);
        c.add(Restrictions.eq("basketId", userId));
        return (Basket) c.uniqueResult();
    }
    
    @Override
    @Transactional
    public void update(Basket user)
    {
        getSession().update(user);
    }
    
    @Override
    public List<Basket> getByUser(User u)
    {
        Criteria c = getSession().createCriteria(Basket.class);
        c.add(Restrictions.eq("basketOwner", u));
        return (List<Basket>) c.list();
    }
    
    @Override
    public int validity(int id)
    {
        Basket b = get(id);
        return b.isCheckOut();
    }
}
