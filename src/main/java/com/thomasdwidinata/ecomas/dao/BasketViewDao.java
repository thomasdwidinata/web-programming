package com.thomasdwidinata.ecomas.dao;

import java.util.List;
import org.springframework.stereotype.Repository;
import com.thomasdwidinata.ecomas.models.*;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author thomasdwidinata
 */
@Repository("BasketViewDao")
public class BasketViewDao extends AbstractDao {
    public List<BasketView> get()
    {
        Criteria a = getSession().createCriteria(BasketView.class);
        return a.list();
    }
    
    public List<BasketView> get(String username)
    {
        Criteria c = getSession().createCriteria(BasketView.class);
        c.add(Restrictions.eq("username", username));
        return (List<BasketView>) c.list();
    }
}
