package com.thomasdwidinata.ecomas.dao;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.thomasdwidinata.ecomas.models.User;

/**
 *
 * @author thomasdwidinata
 */
@Repository("UserDao")
public class UserDaoImpl extends AbstractDao implements UserDao{
    @Override
    @Transactional
    public void add(User user)
    {
        persist(user);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<User> get()
    {
        Criteria c = getSession().createCriteria(User.class);
        return (List<User>) c.list();
    }
    
    @Override
    @Transactional
    public void delete(int id)
    {
        Query q = getSession().createSQLQuery("DELETE FROM `user` WHERE `userId` = :id");
        q.setString("id", String.valueOf(id));
        q.executeUpdate();
    }
    
    @Override
    public User get(int id)
    {
        Criteria c = getSession().createCriteria(User.class);
        c.add(Restrictions.eq("id", id));
        return (User) c.uniqueResult();
    }
    
    @Override
    @Transactional
    public void update(User user)
    {
        getSession().update(user);
    }
    
    @Override
    public User get(String username)
    {
        Criteria c = getSession().createCriteria(User.class);
        c.add(Restrictions.eq("username", username));
        return (User) c.uniqueResult();
    }
    
    @Override
    public void setToken(User user, String token)
    {
        user.setToken(token);
        getSession().update(user);
    }
    
    @Override
    public void revokeToken(User user)
    {
        user.setToken(null);
        getSession().update(user);
    }
    
   
    @Override
    public User salt(String salt)
    {
        Criteria c = getSession().createCriteria(User.class);
        c.add(Restrictions.eq("token", salt));
        return (User) c.uniqueResult();
    }
}
