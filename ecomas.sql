-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 07, 2017 at 07:56 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecomas`
--
CREATE DATABASE IF NOT EXISTS `ecomas` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ecomas`;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `adminId` int(10) UNSIGNED NOT NULL,
  `adminName` varchar(45) NOT NULL,
  `password` varchar(32) NOT NULL,
  `salt` varchar(32) DEFAULT NULL,
  `roleId` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`adminId`, `adminName`, `password`, `salt`, `roleId`) VALUES
(1, 'thomasdwidinata', '301b752f26b2e8a6e6ac5459c046e64f', NULL, 1),
(2, 'glacies_ice', 'andreisthebest', NULL, 1),
(3, 'albertdarmawan', '43cbbe9c73484fba43bc0a34dd64614d', NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `adminrole`
--

DROP TABLE IF EXISTS `adminrole`;
CREATE TABLE `adminrole` (
  `roleId` int(10) UNSIGNED NOT NULL,
  `roleName` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adminrole`
--

INSERT INTO `adminrole` (`roleId`, `roleName`) VALUES
(1, 'All Access'),
(2, 'Category'),
(4, 'Customer Service'),
(3, 'Payment');

-- --------------------------------------------------------

--
-- Table structure for table `basket`
--

DROP TABLE IF EXISTS `basket`;
CREATE TABLE `basket` (
  `basketId` int(10) UNSIGNED NOT NULL,
  `basketOwner` int(10) UNSIGNED NOT NULL,
  `checkedOut` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `basket`
--

INSERT INTO `basket` (`basketId`, `basketOwner`, `checkedOut`) VALUES
(1, 1, 1),
(2, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `basketitems`
--

DROP TABLE IF EXISTS `basketitems`;
CREATE TABLE `basketitems` (
  `basketItemList` int(10) UNSIGNED NOT NULL,
  `basketId` int(10) UNSIGNED NOT NULL,
  `itemId` int(10) UNSIGNED NOT NULL,
  `takenItem` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `basketitems`
--

INSERT INTO `basketitems` (`basketItemList`, `basketId`, `itemId`, `takenItem`) VALUES
(1, 1, 1, 1),
(2, 1, 2, 1),
(3, 1, 3, 0),
(5, 2, 1, 6);

-- --------------------------------------------------------

--
-- Stand-in structure for view `basketview`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `basketview`;
CREATE TABLE `basketview` (
`basketId` int(10) unsigned
,`username` varchar(55)
,`itemName` varchar(100)
,`takenItem` int(10) unsigned
,`checkedOut` tinyint(1)
);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `categoryId` int(10) UNSIGNED NOT NULL,
  `categoryName` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`categoryId`, `categoryName`) VALUES
(1, 'Electronic'),
(2, 'Fashion'),
(3, 'Hobby'),
(4, 'Smartphone'),
(5, 'Mini PC');

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
CREATE TABLE `item` (
  `itemId` int(10) UNSIGNED NOT NULL,
  `categoryId` int(10) UNSIGNED NOT NULL,
  `itemName` varchar(100) NOT NULL,
  `seller` int(10) UNSIGNED NOT NULL,
  `stock` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`itemId`, `categoryId`, `itemName`, `seller`, `stock`) VALUES
(1, 1, 'Raspberry Pi 3', 1, 0),
(2, 4, 'Sony Smartphone', 1, 5),
(3, 5, 'Raspberry Pi 3 Element14', 1, 0);

-- --------------------------------------------------------

--
-- Stand-in structure for view `itemlist`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `itemlist`;
CREATE TABLE `itemlist` (
`itemId` int(10) unsigned
,`categoryName` varchar(55)
,`itemName` varchar(100)
,`username` varchar(55)
,`stock` int(10) unsigned
);

-- --------------------------------------------------------

--
-- Table structure for table `itemuom`
--

DROP TABLE IF EXISTS `itemuom`;
CREATE TABLE `itemuom` (
  `uomId` int(10) UNSIGNED NOT NULL,
  `itemId` int(10) UNSIGNED NOT NULL,
  `unitId` int(10) UNSIGNED NOT NULL,
  `qty` int(10) UNSIGNED NOT NULL,
  `price` double UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `itemuom`
--

INSERT INTO `itemuom` (`uomId`, `itemId`, `unitId`, `qty`, `price`) VALUES
(1, 1, 1, 1, 575000);

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
CREATE TABLE `notification` (
  `notificationId` int(10) UNSIGNED NOT NULL,
  `sender` int(10) UNSIGNED DEFAULT NULL,
  `receiver` int(10) UNSIGNED DEFAULT NULL,
  `adminSender` int(10) UNSIGNED DEFAULT NULL,
  `adminReceiver` int(10) UNSIGNED DEFAULT NULL,
  `subject` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `notificationRead` tinyint(1) UNSIGNED NOT NULL,
  `notificationType` smallint(2) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
CREATE TABLE `payment` (
  `paymentId` int(10) UNSIGNED NOT NULL,
  `paymentName` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`paymentId`, `paymentName`) VALUES
(1, 'Cash on Delivery'),
(3, 'Credit Card'),
(2, 'Transfer');

-- --------------------------------------------------------

--
-- Table structure for table `shipping`
--

DROP TABLE IF EXISTS `shipping`;
CREATE TABLE `shipping` (
  `shippingId` int(10) UNSIGNED NOT NULL,
  `shippingName` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shipping`
--

INSERT INTO `shipping` (`shippingId`, `shippingName`) VALUES
(3, 'GoSend'),
(1, 'JNE'),
(2, 'TIKI');

-- --------------------------------------------------------

--
-- Table structure for table `threadmessages`
--

DROP TABLE IF EXISTS `threadmessages`;
CREATE TABLE `threadmessages` (
  `messageId` int(11) UNSIGNED NOT NULL,
  `userId` int(10) UNSIGNED DEFAULT NULL,
  `threadId` int(10) UNSIGNED NOT NULL,
  `messageContent` text NOT NULL,
  `adminId` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `threadpermission`
--

DROP TABLE IF EXISTS `threadpermission`;
CREATE TABLE `threadpermission` (
  `threadPermissionId` int(10) UNSIGNED NOT NULL,
  `userId` int(10) UNSIGNED NOT NULL,
  `threadId` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `threadtitle`
--

DROP TABLE IF EXISTS `threadtitle`;
CREATE TABLE `threadtitle` (
  `threadId` int(10) UNSIGNED NOT NULL,
  `threadName` varchar(180) NOT NULL,
  `threadTypeId` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `threadtype`
--

DROP TABLE IF EXISTS `threadtype`;
CREATE TABLE `threadtype` (
  `threadTypeId` int(10) UNSIGNED NOT NULL,
  `threadTypeName` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `threadtype`
--

INSERT INTO `threadtype` (`threadTypeId`, `threadTypeName`) VALUES
(1, 'Nego'),
(2, 'Message'),
(3, 'Complain');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
CREATE TABLE `transaction` (
  `transactionId` int(10) UNSIGNED NOT NULL,
  `basketId` int(10) UNSIGNED NOT NULL,
  `shippingId` int(10) UNSIGNED NOT NULL,
  `paymentId` int(10) UNSIGNED NOT NULL,
  `paymentInfo` varchar(100) NOT NULL,
  `address` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`transactionId`, `basketId`, `shippingId`, `paymentId`, `paymentInfo`, `address`) VALUES
(0, 1, 1, 1, '7333733373337333', 'Jalan ayam hantu neraka'),
(2, 1, 2, 3, '33333333', 'jalan wtf ');

-- --------------------------------------------------------

--
-- Table structure for table `transactionhandled`
--

DROP TABLE IF EXISTS `transactionhandled`;
CREATE TABLE `transactionhandled` (
  `transactionHandled` int(10) UNSIGNED NOT NULL,
  `transactionsId` int(10) UNSIGNED NOT NULL,
  `transactionStatusId` int(10) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transactionhandled`
--

INSERT INTO `transactionhandled` (`transactionHandled`, `transactionsId`, `transactionStatusId`, `timestamp`) VALUES
(1, 0, 1, '2017-06-06 22:27:47');

-- --------------------------------------------------------

--
-- Stand-in structure for view `transactions`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `transactions`;
CREATE TABLE `transactions` (
`transactionId` int(10) unsigned
,`userName` varchar(55)
,`shippingName` varchar(45)
,`paymentName` varchar(45)
,`paymentInfo` varchar(100)
,`address` varchar(200)
,`itemName` varchar(100)
);

-- --------------------------------------------------------

--
-- Table structure for table `transactionstatus`
--

DROP TABLE IF EXISTS `transactionstatus`;
CREATE TABLE `transactionstatus` (
  `transactionStatusId` int(10) UNSIGNED NOT NULL,
  `transactionStatusName` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transactionstatus`
--

INSERT INTO `transactionstatus` (`transactionStatusId`, `transactionStatusName`) VALUES
(2, 'Item Delivered'),
(4, 'Item on Complain'),
(3, 'Item Received'),
(5, 'Item Returned'),
(1, 'Payment Received');

-- --------------------------------------------------------

--
-- Stand-in structure for view `transactionstatussystem`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `transactionstatussystem`;
CREATE TABLE `transactionstatussystem` (
`transactionId` int(10) unsigned
,`transactionStatusName` varchar(45)
,`timestamp` datetime
);

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

DROP TABLE IF EXISTS `unit`;
CREATE TABLE `unit` (
  `unitId` int(10) UNSIGNED NOT NULL,
  `unitName` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `unit`
--

INSERT INTO `unit` (`unitId`, `unitName`) VALUES
(3, 'Kg'),
(2, 'Meter'),
(5, 'm^2'),
(4, 'Ons'),
(1, 'Pcs');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `userId` int(10) UNSIGNED NOT NULL,
  `username` varchar(55) NOT NULL,
  `email` varchar(75) NOT NULL,
  `password` varchar(32) NOT NULL,
  `token` varchar(32) DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `address` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userId`, `username`, `email`, `password`, `token`, `created`, `address`) VALUES
(1, 'thomasdwidinata', 'thomasdwidinata@gmail.com', '301b752f26b2e8a6e6ac5459c046e64f', 'UMY745JDSA8VBC15Y7', '2017-05-18 19:20:34', NULL),
(2, 'inipembeli', 'lorem@ipsum.com', '47bce5c74f589f4867dbd57e9ca9f808', NULL, '2017-05-18 19:27:46', NULL),
(10, 'wadefak', 'wadefak@wtf.com', '43cbbe9c73484fba43bc0a34dd64614d', NULL, '2017-05-26 21:38:53', 'Jalan Habib Rizeq Jelek 2 No. 15');

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

DROP TABLE IF EXISTS `wishlist`;
CREATE TABLE `wishlist` (
  `itemId` int(10) UNSIGNED NOT NULL,
  `userId` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure for view `basketview`
--
DROP TABLE IF EXISTS `basketview`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `basketview`  AS  select `b`.`basketId` AS `basketId`,`u`.`username` AS `username`,`i`.`itemName` AS `itemName`,`bi`.`takenItem` AS `takenItem`,`b`.`checkedOut` AS `checkedOut` from (((`basketitems` `bi` join `item` `i`) join `basket` `b`) join `user` `u`) where ((`bi`.`itemId` = `i`.`itemId`) and (`b`.`basketOwner` = `u`.`userId`) and (`bi`.`basketId` = `b`.`basketId`)) ;

-- --------------------------------------------------------

--
-- Structure for view `itemlist`
--
DROP TABLE IF EXISTS `itemlist`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `itemlist`  AS  select `i`.`itemId` AS `itemId`,`c`.`categoryName` AS `categoryName`,`i`.`itemName` AS `itemName`,`u`.`username` AS `username`,`i`.`stock` AS `stock` from ((`item` `i` join `user` `u`) join `category` `c`) where ((`i`.`categoryId` = `c`.`categoryId`) and (`i`.`seller` = `u`.`userId`)) ;

-- --------------------------------------------------------

--
-- Structure for view `transactions`
--
DROP TABLE IF EXISTS `transactions`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `transactions`  AS  select `t`.`transactionId` AS `transactionId`,`u`.`username` AS `userName`,`s`.`shippingName` AS `shippingName`,`p`.`paymentName` AS `paymentName`,`t`.`paymentInfo` AS `paymentInfo`,`t`.`address` AS `address`,`i`.`itemName` AS `itemName` from ((((((`transaction` `t` join `user` `u`) join `shipping` `s`) join `payment` `p`) join `basket` `b`) join `basketitems` `bi`) join `item` `i`) where ((`t`.`shippingId` = `s`.`shippingId`) and (`t`.`paymentId` = `p`.`paymentId`) and (`t`.`basketId` = `b`.`basketId`) and (`b`.`basketOwner` = `u`.`userId`) and (`bi`.`basketId` = `b`.`basketId`) and (`bi`.`itemId` = `i`.`itemId`)) ;

-- --------------------------------------------------------

--
-- Structure for view `transactionstatussystem`
--
DROP TABLE IF EXISTS `transactionstatussystem`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `transactionstatussystem`  AS  select `t`.`transactionId` AS `transactionId`,`tsi`.`transactionStatusName` AS `transactionStatusName`,`th`.`timestamp` AS `timestamp` from ((`transaction` `t` join `transactionhandled` `th`) join `transactionstatus` `tsi`) where ((`t`.`transactionId` = `th`.`transactionsId`) and (`th`.`transactionStatusId` = `tsi`.`transactionStatusId`)) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`adminId`),
  ADD KEY `roleId` (`roleId`);

--
-- Indexes for table `adminrole`
--
ALTER TABLE `adminrole`
  ADD PRIMARY KEY (`roleId`),
  ADD UNIQUE KEY `roleName` (`roleName`);

--
-- Indexes for table `basket`
--
ALTER TABLE `basket`
  ADD PRIMARY KEY (`basketId`),
  ADD KEY `FK_basket_user` (`basketOwner`);

--
-- Indexes for table `basketitems`
--
ALTER TABLE `basketitems`
  ADD PRIMARY KEY (`basketItemList`),
  ADD KEY `itemId` (`itemId`),
  ADD KEY `basketitems_ibfk_3` (`basketId`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`categoryId`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`itemId`),
  ADD KEY `item_ibfk_1` (`categoryId`),
  ADD KEY `seller` (`seller`);

--
-- Indexes for table `itemuom`
--
ALTER TABLE `itemuom`
  ADD PRIMARY KEY (`uomId`),
  ADD KEY `itemId` (`itemId`),
  ADD KEY `unitId` (`unitId`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`notificationId`),
  ADD KEY `sender` (`sender`),
  ADD KEY `receiver` (`receiver`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`paymentId`),
  ADD UNIQUE KEY `paymentName` (`paymentName`);

--
-- Indexes for table `shipping`
--
ALTER TABLE `shipping`
  ADD PRIMARY KEY (`shippingId`),
  ADD UNIQUE KEY `shippingName` (`shippingName`);

--
-- Indexes for table `threadmessages`
--
ALTER TABLE `threadmessages`
  ADD PRIMARY KEY (`messageId`),
  ADD KEY `userId` (`userId`),
  ADD KEY `threadId` (`threadId`),
  ADD KEY `threadmessages_ibfk_3` (`adminId`);

--
-- Indexes for table `threadpermission`
--
ALTER TABLE `threadpermission`
  ADD PRIMARY KEY (`threadPermissionId`),
  ADD KEY `userId` (`userId`),
  ADD KEY `threadId` (`threadId`);

--
-- Indexes for table `threadtitle`
--
ALTER TABLE `threadtitle`
  ADD PRIMARY KEY (`threadId`),
  ADD KEY `threadTypeId` (`threadTypeId`);

--
-- Indexes for table `threadtype`
--
ALTER TABLE `threadtype`
  ADD PRIMARY KEY (`threadTypeId`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`transactionId`),
  ADD KEY `shippingId` (`shippingId`),
  ADD KEY `paymentId` (`paymentId`),
  ADD KEY `basketId` (`basketId`);

--
-- Indexes for table `transactionhandled`
--
ALTER TABLE `transactionhandled`
  ADD PRIMARY KEY (`transactionHandled`),
  ADD KEY `transactionStatusId` (`transactionStatusId`),
  ADD KEY `transactionsId` (`transactionsId`);

--
-- Indexes for table `transactionstatus`
--
ALTER TABLE `transactionstatus`
  ADD PRIMARY KEY (`transactionStatusId`),
  ADD UNIQUE KEY `transactionStatusName` (`transactionStatusName`);

--
-- Indexes for table `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`unitId`),
  ADD UNIQUE KEY `unitName` (`unitName`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userId`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`userId`),
  ADD UNIQUE KEY `itemId` (`itemId`),
  ADD UNIQUE KEY `userId` (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `adminId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `adminrole`
--
ALTER TABLE `adminrole`
  MODIFY `roleId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `basket`
--
ALTER TABLE `basket`
  MODIFY `basketId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `basketitems`
--
ALTER TABLE `basketitems`
  MODIFY `basketItemList` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `categoryId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
  MODIFY `itemId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `itemuom`
--
ALTER TABLE `itemuom`
  MODIFY `uomId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `notificationId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `paymentId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `shipping`
--
ALTER TABLE `shipping`
  MODIFY `shippingId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `threadmessages`
--
ALTER TABLE `threadmessages`
  MODIFY `messageId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `threadpermission`
--
ALTER TABLE `threadpermission`
  MODIFY `threadPermissionId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `threadtitle`
--
ALTER TABLE `threadtitle`
  MODIFY `threadId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `threadtype`
--
ALTER TABLE `threadtype`
  MODIFY `threadTypeId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `transactionId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `transactionhandled`
--
ALTER TABLE `transactionhandled`
  MODIFY `transactionHandled` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `transactionstatus`
--
ALTER TABLE `transactionstatus`
  MODIFY `transactionStatusId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `unit`
--
ALTER TABLE `unit`
  MODIFY `unitId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `userId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin`
--
ALTER TABLE `admin`
  ADD CONSTRAINT `admin_ibfk_1` FOREIGN KEY (`roleId`) REFERENCES `adminrole` (`roleId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `basket`
--
ALTER TABLE `basket`
  ADD CONSTRAINT `FK_basket_user` FOREIGN KEY (`basketOwner`) REFERENCES `user` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `basketitems`
--
ALTER TABLE `basketitems`
  ADD CONSTRAINT `basketitems_ibfk_2` FOREIGN KEY (`itemId`) REFERENCES `item` (`itemId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `basketitems_ibfk_3` FOREIGN KEY (`basketId`) REFERENCES `basket` (`basketId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `item`
--
ALTER TABLE `item`
  ADD CONSTRAINT `item_ibfk_1` FOREIGN KEY (`categoryId`) REFERENCES `category` (`categoryId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `item_ibfk_2` FOREIGN KEY (`seller`) REFERENCES `user` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `itemuom`
--
ALTER TABLE `itemuom`
  ADD CONSTRAINT `itemuom_ibfk_1` FOREIGN KEY (`itemId`) REFERENCES `item` (`itemId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `notification`
--
ALTER TABLE `notification`
  ADD CONSTRAINT `notification_ibfk_1` FOREIGN KEY (`sender`) REFERENCES `user` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `notification_ibfk_2` FOREIGN KEY (`receiver`) REFERENCES `user` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `threadmessages`
--
ALTER TABLE `threadmessages`
  ADD CONSTRAINT `threadmessages_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `threadmessages_ibfk_2` FOREIGN KEY (`threadId`) REFERENCES `threadtitle` (`threadId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `threadmessages_ibfk_3` FOREIGN KEY (`adminId`) REFERENCES `admin` (`adminId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `threadpermission`
--
ALTER TABLE `threadpermission`
  ADD CONSTRAINT `threadpermission_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `threadpermission_ibfk_2` FOREIGN KEY (`threadId`) REFERENCES `threadtitle` (`threadId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `threadtitle`
--
ALTER TABLE `threadtitle`
  ADD CONSTRAINT `threadtitle_ibfk_4` FOREIGN KEY (`threadTypeId`) REFERENCES `threadtype` (`threadTypeId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transaction`
--
ALTER TABLE `transaction`
  ADD CONSTRAINT `transaction_ibfk_2` FOREIGN KEY (`shippingId`) REFERENCES `shipping` (`shippingId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transaction_ibfk_4` FOREIGN KEY (`paymentId`) REFERENCES `payment` (`paymentId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transaction_ibfk_5` FOREIGN KEY (`basketId`) REFERENCES `basket` (`basketId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transactionhandled`
--
ALTER TABLE `transactionhandled`
  ADD CONSTRAINT `transactionhandled_ibfk_2` FOREIGN KEY (`transactionStatusId`) REFERENCES `transactionstatus` (`transactionStatusId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transactionhandled_ibfk_3` FOREIGN KEY (`transactionsId`) REFERENCES `transaction` (`transactionId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD CONSTRAINT `wishlist_ibfk_1` FOREIGN KEY (`itemId`) REFERENCES `item` (`itemId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `wishlist_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
